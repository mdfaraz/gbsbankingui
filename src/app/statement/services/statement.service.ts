import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Statement, StatementRequest } from '../models/statement';
import { Crypto } from 'src/app/core/common/services/crypto';


@Injectable({
  providedIn: 'root'
})
export class StatementService {

  private baseUrl = environment.baseUrl+'/api/v1/bankstatement';

  constructor(private http : HttpClient) { }

  getStatement(statementRequest: StatementRequest): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(statementRequest))
    };
    
    return this.http.post(this.baseUrl + `/detail`, statementRequest, {headers});
  }

  getPaginationStatement(statementRequest: StatementRequest): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(statementRequest))
    };
    
    return this.http.post(this.baseUrl + `/pagination`, statementRequest, {headers});
  }

}
