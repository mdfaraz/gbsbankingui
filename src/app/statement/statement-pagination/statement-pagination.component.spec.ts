import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatementPaginationComponent } from './statement-pagination.component';

describe('StatementPaginationComponent', () => {
  let component: StatementPaginationComponent;
  let fixture: ComponentFixture<StatementPaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatementPaginationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatementPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
