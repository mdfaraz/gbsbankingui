import { AfterViewInit, Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Constants } from 'src/app/core/common/models/constants';
import { ConfigService } from 'src/app/core/common/services/config.service';
import { Statement, StatementRequest, TransactionRecords } from '../models/statement';
import { StatementService } from '../services/statement.service';
import { map } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-statement-pagination',
  templateUrl: './statement-pagination.component.html',
  styleUrls: ['./statement-pagination.component.css']
})
export class StatementPaginationComponent {

  public txnRecords : Array<TransactionRecords> = new Array<TransactionRecords>();
  private rowSelection;
  public statement : Statement = new Statement();
  @Input() public statementRequest : StatementRequest = new StatementRequest();

  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;

  displayedColumns: string[] = ['transactionId','type','txnDate','amount','balance', 'remarks'];
  dataSource = new MatTableDataSource<TransactionRecords>();

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  @ViewChild(MatSort) sort: MatSort | undefined;

  public typeIdMap : Map<string, string> = Constants.typeMap();

  constructor(private statementService: StatementService,
              private configService:ConfigService,
              private router: Router) { 
    this.rowSelection = "single";
    
  }

  public firstRow : TransactionRecords = new TransactionRecords();
  public lastRow : TransactionRecords = new TransactionRecords();

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator!
    this.dataSource.sort = this.sort!;
    this.initialLoad();
    //this.pageChange();
  }

  initialLoad() {
      console.log(this.statementRequest);
      if (this.statementRequest != null && this.statementRequest != undefined) 
      {
        this.statementRequest.conflg = false;
        this.statementService
        .getPaginationStatement(this.statementRequest)
        .subscribe(data => {
          if (data != null && data.txnRecords != null && data.txnRecords != undefined) {
            this.dataSource.data = data.txnRecords;
            this.firstRow = data.txnRecords[0];

            let lastIndex = data.txnRecords.length -1;
            this.lastRow = data.txnRecords[lastIndex];
          }
        });
      }
  }

  handlePage(event?:PageEvent) {
    this.statementRequest.conflg = true;
    this.statementRequest.lastTxnId = this.lastRow.transactionId;
    this.statementService
    .getPaginationStatement(this.statementRequest)
    .subscribe(data => {
      if (data != null && data.txnRecords != null && data.txnRecords != undefined) {
        let transactionRecords : Array<TransactionRecords> = data.txnRecords; 
        this.dataSource.data = transactionRecords;
        this.firstRow = data.txnRecords[0];

        let lastIndex = data.txnRecords.length -1;
        this.lastRow = data.txnRecords[lastIndex];
      }
    });
  }

  ngOnChanges(): void 
  {
    console.log(this.statementRequest);
    this.statementService.getStatement(this.statementRequest)
    .subscribe(data => {
        console.log(data);
        this.statement = data;
        this.txnRecords = this.statement.txnRecords!;
        this.dataSource = new MatTableDataSource<TransactionRecords>(this.txnRecords);
    });
  }

  applyFilter(event : any) : void {
    let filterValue = event.target.value;
    console.log(filterValue);
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
