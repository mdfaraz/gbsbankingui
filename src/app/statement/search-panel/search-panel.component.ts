import { Component, OnInit, Output, EventEmitter, OnChanges  } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/core/common/services/config.service';
import { StatementRequest } from '../models/statement';
import { DatePipe } from '@angular/common';
import { RegistrationService } from 'src/app/registration/services/registration.service';
import { Constants } from 'src/app/core/common/models/constants';
import { UserStatus } from 'src/app/registration/models/user-status';
import { PartnerConfig } from 'src/app/core/common/models/partner-config';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { Account } from 'src/app/transaction/models/account';
import { AccountService } from 'src/app/transaction/services/account.service';

@Component({
  selector: 'app-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.css']
})
export class SearchPanelComponent implements OnInit {

  public filterStatementForm = new FormGroup({
    accountId: new FormControl('', [Validators.required]),
    start: new FormControl(),
    end: new FormControl()
  });

  public appUserId : string = this.dataSharingService.getAppUserId();
  public appName : string = this.dataSharingService.getAppName();
  public partnerIdNameMap = new Map();
  public partners = new Array<PartnerConfig>();
  public accountBalance : number | undefined;

  public errorMessage: string = '';
  public isError: boolean = false;

  public today = new Date();
  
  public statementRequest = new StatementRequest();
  @Output() emitterStatement = new EventEmitter<StatementRequest>();

  public userIdMap = new Map();
  public userStatusList: Array<UserStatus> = new Array<UserStatus>();
  public selectedUser : UserStatus = new UserStatus();
  public primaryAccountUser : UserStatus = new UserStatus();

  public selectedUserBalance : string = '';

  constructor(private configService: ConfigService,
    private registrationService : RegistrationService,
    private datepipe : DatePipe,
    private dataSharingService : DataSharingService,
    private accountService : AccountService,
    private router: Router) { }

  get f() { return this.filterStatementForm.controls; } 

  ngOnInit(): void {
    this.configService.listPartner().subscribe(data => {
      this.partners = data;
      this.partners.forEach(e => {
          this.partnerIdNameMap.set(e.partnerId, e.partnerName);
      });
    });
    console.log("appName=["+this.appName+"] --- appUserId=["+this.appUserId+"]");

    this.registrationService.getAllRegistrations(this.appName, this.appUserId)
    .subscribe(data => {
        this.userStatusList = data;
        console.log(data);
        this.userStatusList.forEach(e => {
          let key = e.corpId+"-"+e.userId;
          this.userIdMap.set(key, e);
          if (e.primaryAccount) {
            let value = e.corpId+'-'+e.userId;
            this.autoPopulateForm(value);
            this.selectedUser = e;
            this.setAccountBalance();
            this.onSubmit();
          }
        });
    });
  }

  onChange(event : any) : void {
      let eventKey = event.value;
      this.selectedUser = this.userIdMap.get(eventKey);
      this.setAccountBalance();      
  }

  public setAccountBalance() : void {
    if (this.selectedUser != undefined && this.selectedUser != null) {
      let accountReq : Account = new Account();
      accountReq.userId = this.selectedUser.userId;
      accountReq.appUserId = this.appUserId;
      accountReq.appType = Constants.APP_TYPE;
      accountReq.corpId = this.selectedUser.corpId;
      accountReq.partnerId = this.selectedUser.partnerId;
      accountReq.urn = this.selectedUser.urnId;
      accountReq.accountNumber = this.selectedUser.accountNumber;
      this.accountService.accountBalance(accountReq)
      .subscribe(data => {

          if (data["error"] == undefined) {
            let accountResp : Account = data;
            console.log(accountResp); 
            this.accountBalance = accountResp.balance;
          } else {
            this.accountBalance = 0;
          }
      });
    }
    else {
      this.accountBalance = 0;
    }
  }

  onSubmit(): void {
    if (this.filterStatementForm.invalid) {
      console.log("Form is Invalid - Exiting");
      return;
    }

    this.statementRequest.appUserId = this.appUserId;
    this.statementRequest.appType = Constants.APP_TYPE;
    this.statementRequest.userId = this.selectedUser.userId;
    this.statementRequest.corpId = this.selectedUser.corpId;
    this.statementRequest.partnerId = this.selectedUser.partnerId;
    this.statementRequest.accountNo = this.selectedUser.accountNumber;
    this.statementRequest.urnId = this.selectedUser.urnId;
    let fromDate = this.datepipe.transform(this.f.start.value, 'dd-MM-yyyy');
    let toDate = this.datepipe.transform(this.f.end.value, 'dd-MM-yyyy');
    this.statementRequest.fromDate = fromDate != null ? fromDate : '01-01-2000';
    this.statementRequest.toDate = toDate != null ? toDate : '01-01-2000';
    this.statementRequest.conflg = false;
    //this.statementRequest.lastTxnId = 
    console.log(this.statementRequest);

    this.emitterStatement.emit(this.statementRequest);
  }

  addUser() : void {
    this.router.navigate(['/register-landing']);
  }

  mask(sensitive : string | undefined) : string {
    let format : string = 'XXXX XXXX XXXX';
    let maskedValue : string = '';
    if (sensitive != null && sensitive.length > 6) {
        let actualLength = sensitive.length;
        maskedValue = format.slice(0, actualLength - 2) + sensitive.slice(actualLength-2, actualLength) ;
    }
    return maskedValue;
  }

  public isUserInActiveORNotRegistered(userStatus : UserStatus) : boolean {
    return !userStatus.active || userStatus.currentState != 'REGISTERED';
  }

  autoPopulateForm(account : string) : void {
    this.f.accountId.setValue(account);
    var currentDate = new Date();
    let day = 24 * 3600 * 1000;
    let previousDate = new Date(currentDate.getTime() - (10 * day));
    //console.log(currentDate + '=====' + previousDate);
    this.f.start.setValue(previousDate);
    this.f.end.setValue(currentDate);
    //console.log(this.f.start.value + "-==== "+ this.f.end.value);
  } 

}
