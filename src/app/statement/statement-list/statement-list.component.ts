import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/core/common/services/config.service';
import { Statement, StatementRequest, TransactionRecords } from '../models/statement';
import { StatementService } from '../services/statement.service';

@Component({
  selector: 'app-statement-list',
  templateUrl: './statement-list.component.html',
  styleUrls: ['./statement-list.component.css']
})
export class StatementListComponent implements OnChanges {

  public txnRecords : Array<TransactionRecords> = new Array<TransactionRecords>();
  private rowSelection;
  public statement : Statement = new Statement();
  @Input() public statementRequest : StatementRequest = new StatementRequest();
  @Input() public updatedTs : any; 

  displayedColumns: string[] = ['txnDate',
                                 'remarks',
                                 'type',
                                 'amount',
                                 'balance',
                                 'transactionId'];

  dataSource : TransactionRecords[] = [];

  public typeIdMap : Map<string, string> = this.typeMap();

  public displayStatementTable : boolean = false;
  public isError : boolean = false;
  public errorMessage : string = '';

  constructor(private statementService: StatementService,
              private configService:ConfigService,
              private router: Router) { 
    this.rowSelection = "single";
  }

  ngOnChanges(changes: SimpleChanges): void 
  {
    console.log("----ngOnChanges -- child ----- ");
    console.log(this.statementRequest);
    if (Object.keys(this.statementRequest).length != 0) 
    {
      this.statementService.getPaginationStatement(this.statementRequest)
      .subscribe(data => {
          console.log(data);

          let error = data["error"];
          if (error == undefined) 
          {
            this.displayStatementTable = true;
            this.errorMessage = '';
            this.isError = false;

            this.statement = data;
            this.txnRecords = this.statement.txnRecords!;
            this.dataSource = this.txnRecords;
          }
          else
          { 
            this.isError = true;
            this.errorMessage = error.errorMessage;
            this.displayStatementTable = false;
          }
      });
    }
  }

  typeMap(): Map<string, string> {
    let typeMap : Map<string, string> = new Map<string, string>();
    typeMap.set('CR', 'Credit');
    typeMap.set('DR','Debit');
    return typeMap;
  }
}
