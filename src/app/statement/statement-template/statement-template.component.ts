import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { StatementRequest } from '../models/statement';

@Component({
  selector: 'app-statement-template',
  templateUrl: './statement-template.component.html',
  styleUrls: ['./statement-template.component.css']
})
export class StatementTemplateComponent implements OnChanges {

  constructor() { }
  public statementAppliedFilter : StatementRequest = new StatementRequest();
  public updatedTimestamp : any;

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
      console.log("---- ngOnChnages ::: updated in parent-----");
      console.log(this.statementAppliedFilter);
      console.log("---------------------------")
  }

  appliedFilterInSearchPanel(statementRequestFromFilter : StatementRequest) {
      console.log("---- appliedFilterInSearchPanel() ::: updated in parent-----");
      console.log(statementRequestFromFilter);
      let temp = statementRequestFromFilter;
      this.statementAppliedFilter = temp;
      this.updatedTimestamp = Date.now();
  }

}
