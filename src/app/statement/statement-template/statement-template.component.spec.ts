import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatementTemplateComponent } from './statement-template.component';

describe('StatementTemplateComponent', () => {
  let component: StatementTemplateComponent;
  let fixture: ComponentFixture<StatementTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatementTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatementTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
