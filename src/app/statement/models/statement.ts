export class Statement {
    userId? : string;
    appUserId? : string;
    appType? : string;
    corpId? : string;
    accountNo? : string;
    urnNumber? : string;
    txnRecords? : Array<TransactionRecords>
}

export class TransactionRecords {
    transactionId? : string;
    type? : string;
    txnDate? : Date;
    amount? : number;
    balance? : number;
    remarks? : string;
    checknumber? : string;
}

export class StatementRequest {
    userId? : string;
    partnerId? : number;
    appUserId? : string;
    appType? : string;
    corpId? : string;
    accountNo? : string;
    fromDate? : string;
    toDate? : string;
    urnId? : string;
    conflg? : boolean;
    lastTxnId? : string;
}