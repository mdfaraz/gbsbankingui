import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CreatePinComponent } from './pin/create-pin/create-pin.component';
import { VerifyPinComponent } from './pin/verify-pin/verify-pin.component';
import { CreateRegistrationComponent } from './registration/create-registration/create-registration.component';
import { RegistrationListComponent } from './registration/registration-list/registration-list.component';
import { GenerateOtpComponent } from './transaction/generate-otp/generate-otp.component';
import { CreateTransactionComponent } from './transaction/create-transaction/create-transaction.component';
import { TransactionStatusComponent } from './transaction/transaction-status/transaction-status.component';
import { TxnTrainComponent } from './transaction/txn-train/txn-train.component';
import { StatementListComponent } from './statement/statement-list/statement-list.component';
import { StatementTemplateComponent } from './statement/statement-template/statement-template.component';
import { SearchPanelComponent } from './statement/search-panel/search-panel.component';
import { PinTemplateComponent } from './pin/pin-template/pin-template.component';
import { BeneficiaryListComponent } from './registration/beneficiary-list/beneficiary-list.component';
import { CreateBeneficiaryComponent } from './registration/create-beneficiary/create-beneficiary.component';
import { LogoutComponent } from './core/logout/logout.component';
import { RegistrationLandingComponent } from './registration/registration-landing/registration-landing.component';
import { RegistrationTemplateComponent } from './registration/registration-template/registration-template.component';
import { RegistrationStepsComponent } from './registration/registration-steps/registration-steps.component';
import { CallbackComponent } from './core/callback/callback.component';
import { ContactUsComponent } from './core/contact-us/contact-us.component';
import { AddBeneficiaryComponent } from './registration/add-beneficiary/add-beneficiary.component';
import { LoginGuard } from './core/guard/login.guard';
import { DashboardTemplateComponent } from './dashboard/dashboard-template/dashboard-template.component';
import { ResetPinComponent } from './pin/reset-pin/reset-pin.component';

const routes: Routes = [
  { path: 'createPin', component: CreatePinComponent },
  { path: 'pin/:encryptedStr', component: PinTemplateComponent},
  { path: 'verifyPin', component: VerifyPinComponent },
  { path: 'reset-pin', component: ResetPinComponent },
  { path: 'registerUser', component: CreateRegistrationComponent , canActivate: [LoginGuard]},
  { path: 'register-list', component: RegistrationListComponent , canActivate: [LoginGuard]},
  { path: 'create-beneficiary', component: AddBeneficiaryComponent, canActivate: [LoginGuard]},
  { path: 'manage_beneficiary', component: BeneficiaryListComponent, canActivate: [LoginGuard]},
  { path: 'generate-otp', component: GenerateOtpComponent, canActivate: [LoginGuard] },
  { path: 'initiateTxn', component: CreateTransactionComponent, canActivate: [LoginGuard] },
  { path: 'txn-status', component: TransactionStatusComponent, canActivate: [LoginGuard] },
  { path: 'txn-train', component: TxnTrainComponent , canActivate: [LoginGuard]},
  { path: 'search-statement', component: SearchPanelComponent, canActivate: [LoginGuard] },
  { path: 'statement-list', component: StatementListComponent, canActivate: [LoginGuard] },
  { path: 'statement', component: StatementTemplateComponent, canActivate: [LoginGuard]},
  { path: 'dashboard', component: DashboardTemplateComponent, canActivate: [LoginGuard]},
  { path: 'logout', component: LogoutComponent},
  { path: 'register-landing', component: RegistrationLandingComponent, canActivate: [LoginGuard]},
  { path: 'registration-template', component : RegistrationTemplateComponent, canActivate: [LoginGuard]},
  { path: 'registration-steps', component : RegistrationStepsComponent, canActivate: [LoginGuard]},
  { path: 'contact-us', component : ContactUsComponent, canActivate: [LoginGuard]},
  { path: 'costipro', component : CallbackComponent},
  { path: '', redirectTo: '/pin', pathMatch: 'full' }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { enableTracing: true })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
