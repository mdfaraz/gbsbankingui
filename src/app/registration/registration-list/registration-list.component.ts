import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Constants } from 'src/app/core/common/models/constants';
import { PartnerConfig } from 'src/app/core/common/models/partner-config';
import { ConfigService } from 'src/app/core/common/services/config.service';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { User } from '../models/user';
import { UserStatus } from '../models/user-status';
import { RegistrationService } from '../services/registration.service';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-registration-list',
  templateUrl: './registration-list.component.html',
  styleUrls: ['./registration-list.component.css']
})
export class RegistrationListComponent implements OnInit {

  public userStatusList: Array<UserStatus> = new Array<UserStatus>();
  private rowSelection;
  private appType: string = Constants.APP_TYPE;
  public appUserId : string = this.dataSharingService.getAppUserId();
  public appName : string = this.dataSharingService.getAppName();
  public partnerIdNameMap = new Map();
  private partners = new Array<PartnerConfig>();
  public statusIDMap : Map<string, string> = Constants.statusMap();

  displayedColumns: string[] = [
    'partnerName',
    'primary',
    'userId',
    'urnId',
    'accountNumber',
    'aliasId',
    'corpId',
    'status',
    'registrationDate',
    'updateTs',
    'active'];

  public dataSource: UserStatus[] = new Array<UserStatus>();

  constructor(private registrationService: RegistrationService,
              private configService: ConfigService,
              private dataSharingService : DataSharingService,
              private dialog : MatDialog,
              private router: Router) 
  {
    this.rowSelection = "single";
  }

  ngOnInit(): void {
    this.loadRegistrations();
  }

  isActive(userStatus :UserStatus) : boolean {
    return userStatus.active ? true : false;
  }

  deregister(userStatus :UserStatus) : void {
    var user : User = new User();
    user.userId = userStatus.userId;
    user.partnerId = userStatus.partnerId;
    user.appUserId = userStatus.appUserId;
    user.appName = userStatus.appName;
    user.appType = userStatus.appType;
    user.corpId = userStatus.corpId;
    user.aliasId = userStatus.aliasId;
    user.primaryAccount = userStatus.primaryAccount;
    this.openDeleteDialog(user);
  }

  addUser() : void {
    console.log("---- add user ---")
    this.router.navigate(['/register-landing']);
  }

  /*
  statusMap() : Map<string, string> {
    let statusMap : Map<string, string> = new Map<string, string>();
    statusMap.set('REGISTERED', 'Registered');
    statusMap.set('DEREGISTERED', 'De-registered');
    statusMap.set('PENDING_SELF_APPROVAL', 'Pending');
    return statusMap;
  }*/

  loadRegistrations() : void {
    this.registrationService.getAllRegistrations(this.appName, this.appUserId)
      .subscribe(data => {
        console.log(data);

        this.userStatusList = data;
        console.log(this.userStatusList)

        this.configService.listPartner()
          .subscribe(d => {
            console.log(d);
            this.partners = d;

            this.partners.forEach((e) => {
              this.partnerIdNameMap.set(e.partnerId, e.partnerName);
            });
          })
        this.dataSource = this.userStatusList;
      });
  }

  viewProcess() : void {
    console.log("---- view process ---")
    this.router.navigate(['/registration-steps']);
  }

  openDeleteDialog(user :User): void {
    const dRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: {message:"Do you want to De-register the approved user account, <br> Your action is not revokable – User account will also get De-register from ICICI Bank net banking for Integration once “Yes”?"}
    });
    dRef.afterClosed().subscribe(result => {
       if(result.data){
        console.log('Yes clicked');
        this.registrationService.deregisterUser(user).subscribe(data => {
          console.log(data);
          console.log("user de-registered..")
          //this.router.navigate(['/register-list']);
          this.loadRegistrations();
        });
      }
    });
  }

  isPrimaryAccount(userStatus : UserStatus) : Boolean {
     return userStatus.primaryAccount!;
  }

  setPrimaryAccount(user : User): void {
    const dRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: {message:"Do you want to mark this user account as primary account, <br> Other accounts will not remain primary. To continue, Please press “Yes” ?"}
    });
    dRef.afterClosed().subscribe(result => {
       if(result.data){
        console.log('Yes clicked for making this primary account');
        this.registrationService.setPrimaryAccount(user)
        .subscribe(data => {
          let userStatusList : Array<UserStatus> = data;
          this.dataSource = userStatusList;
        });
      }
    });
  }
}
