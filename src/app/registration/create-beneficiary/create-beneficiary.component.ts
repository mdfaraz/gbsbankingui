import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Constants } from 'src/app/core/common/models/constants';
import { ConfigService } from 'src/app/core/common/services/config.service';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { GenerateOtpComponent } from 'src/app/transaction/generate-otp/generate-otp.component';
import { Beneficiary } from '../models/beneficiary';
import { User } from '../models/user';
import { UserStatus } from '../models/user-status';
import { BeneficiaryService } from '../services/beneficiary.service';
import { RegistrationService } from '../services/registration.service';

export interface DialogData {
  selectedUser? : UserStatus;
}

@Component({
  selector: 'app-create-beneficiary',
  templateUrl: './create-beneficiary.component.html',
  styleUrls: ['./create-beneficiary.component.css']
})
export class CreateBeneficiaryComponent implements OnInit {

  public createBeneficiaryForm = new FormGroup({
    accountNumber: new FormControl('', [Validators.required, 
        Validators.minLength(12)]),
    cnfAccountNumber : new FormControl('', [Validators.required,
      Validators.minLength(12)]),
    bankName: new FormControl('', [Validators.required]),
    ifscCode: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
  });

  public beneficiary: Beneficiary = new Beneficiary();
  public partners: any;
  private appType: string = Constants.APP_TYPE;
  public appUserId: string = this.dataSharingService.getAppUserId();
  public appName: string = this.dataSharingService.getAppName();

  public selectedUser: UserStatus = new UserStatus();
  public displayIfscCode: boolean = true;
  public hide = true;

  public errorMessage: string = '';
  public isError: boolean = false;

  constructor(private beneficiaryService: BeneficiaryService,
    private registrationService: RegistrationService,
    private dataSharingService: DataSharingService,
    public dialogRef: MatDialogRef<GenerateOtpComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private route: Router) {

  }

  get f() { return this.createBeneficiaryForm.controls; }

  ngOnInit(): void {
     this.selectedUser = this.data.selectedUser!;
     console.log(this.selectedUser);
  }

  onSubmit() {
    this.isError = false;
    this.errorMessage = '';

    console.log(this.createBeneficiaryForm)
    if (this.createBeneficiaryForm.invalid) {
      let textMessage = '';
      const controls = this.f;
      for (const name in controls) {
          if (controls[name].invalid) {
            textMessage = "Please enter valid value for : " + name;
            break;
          }
      }

      this.errorMessage = textMessage;
      this.isError = true;
      return;
    }

    if (this.f.accountNumber.value != this.f.cnfAccountNumber.value) {
      this.isError = true;
      this.errorMessage = "Mismatch in Account number";
      return;
    }

    this.beneficiary.appUserId = this.appUserId;
    this.beneficiary.appName = this.appName;
    this.beneficiary.corpId = this.selectedUser.corpId;
    this.beneficiary.userId = this.selectedUser.userId;
    this.beneficiary.name = this.f.name.value;
    this.beneficiary.accountNumber = this.f.accountNumber.value;
    this.beneficiary.bankName = this.f.bankName.value;
    this.beneficiary.ifscCode = this.displayIfscCode ?
      this.f.ifscCode.value :
      Constants.ICICI_BANK_IFSC_CODE;
      
    this.beneficiaryService.addBeneficiary(this.beneficiary)
      .subscribe(data => {
        console.log(data);

        let error = data["error"];
        if (error != undefined) {
          this.isError = true;
          this.errorMessage = error.errorCode + " = " + error.errorMessage;
        } else {
          //this.route.navigate(['manage_beneficiary']);
          this.dialogRef.close();
        }

      })

  }

  /*onChange(event: any): void {
    let userId = event.value;
    this.selectedUser = this.userIdMap.get(userId);
  }*/

  onBankChange(event: any): void {
    let bankNameId = event.value;
    if (bankNameId == Constants.PARTNER_ICICI) {
      this.displayIfscCode = false;
      this.f.ifscCode.setValue(Constants.ICICI_BANK_IFSC_CODE);
    }
    else {
      this.displayIfscCode = true;
      this.f.ifscCode.setValue('');
    }
  }

  back() {
    //this.route.navigate(['/home']);
    this.dialogRef.close();
  }

  mask(sensitive : string | undefined) : string {
    let format : string = 'XXXX XXXX XXXX';
    let maskedValue : string = '';
    if (sensitive != null && sensitive.length > 6) {
        let actualLength = sensitive.length;
        maskedValue = format.slice(0, actualLength - 2) + sensitive.slice(actualLength-2, actualLength) ;
    }
    return maskedValue;
  }

}
