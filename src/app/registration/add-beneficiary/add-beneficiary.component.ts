import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from 'src/app/core/common/models/constants';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { BeneficiaryService } from '../services/beneficiary.service';
import { RegistrationService } from '../services/registration.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Beneficiary } from '../models/beneficiary';
import { UserStatus } from '../models/user-status';

@Component({
  selector: 'app-add-beneficiary',
  templateUrl: './add-beneficiary.component.html',
  styleUrls: ['./add-beneficiary.component.css']
})
export class AddBeneficiaryComponent implements OnInit {

  public createBeneficiaryForm = new FormGroup({
    accountId : new FormControl('', [Validators.required]),
    accountNumber: new FormControl('', [Validators.required,
      Validators.minLength(12)]),
    cnfAccountNumber : new FormControl('', [Validators.required,
      Validators.minLength(12)]),
    bankName: new FormControl('', [Validators.required]),
    ifscCode: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
  });

  public beneficiary: Beneficiary = new Beneficiary();
  public partners: any;
  private appType: string = Constants.APP_TYPE;
  public appUserId: string = this.dataSharingService.getAppUserId();
  public appName: string = this.dataSharingService.getAppName();

  public userIdMap = new Map();
  public userStatusList: Array<UserStatus> = new Array<UserStatus>();
  public selectedUser: UserStatus = new UserStatus();
  public displayIfscCode: boolean = true;

  public errorMessage: string = '';
  public isError: boolean = false;

  constructor(private beneficiaryService: BeneficiaryService,
    private registrationService: RegistrationService,
    private dataSharingService: DataSharingService,
    private route: Router) {

  }

  get f() { return this.createBeneficiaryForm.controls; }

  ngOnInit(): void {
    this.registrationService.getAllRegistrations(this.appName, this.appUserId)
      .subscribe(data => {
        this.userStatusList = data;
        console.log(data);
        this.userStatusList.forEach(e => {
          let key = e.corpId+"-"+e.userId;
          this.userIdMap.set(key, e);
        });
      });
      
  }

  onSubmit() {
    this.isError = false;
    this.errorMessage = '';

    console.log(this.createBeneficiaryForm)
    if (this.createBeneficiaryForm.invalid) {
      let textMessage = '';
      const controls = this.f;
      for (const name in controls) {
          if (controls[name].invalid) {
            textMessage = "Please enter valid value for : " + name;
            break;
          }
      }

      this.errorMessage = textMessage;
      this.isError = true;
      return;
    }

    if (this.f.accountNumber.value != this.f.cnfAccountNumber.value) {
      this.isError = true;
      this.errorMessage = "Mismatch in Account number";
      return;
    }

    this.beneficiary.appUserId = this.appUserId;
    this.beneficiary.appName = this.appName;
    this.beneficiary.userId = this.selectedUser.userId;
    this.beneficiary.corpId = this.selectedUser.corpId;
    this.beneficiary.name = this.f.name.value;
    this.beneficiary.accountNumber = this.f.accountNumber.value;
    this.beneficiary.bankName = this.f.bankName.value;
    this.beneficiary.ifscCode = this.displayIfscCode ?
      this.f.ifscCode.value :
      Constants.ICICI_BANK_IFSC_CODE;
      
    this.beneficiaryService.addBeneficiary(this.beneficiary)
      .subscribe(data => {
        console.log(data);

        let error = data["error"];
        if (error != undefined) {
          this.isError = true;
          this.errorMessage = error.errorCode + " = " + error.errorMessage;
        } else {
          this.route.navigate(['manage_beneficiary']);
        }

      })

  }

  onChange(event: any): void {
    let eventKey = event.value;
    this.selectedUser = this.userIdMap.get(eventKey);
  }

  onBankChange(event: any): void {
    let bankNameId = event.value;
    if (bankNameId == Constants.PARTNER_ICICI) {
      this.displayIfscCode = false;
      this.f.ifscCode.setValue(Constants.ICICI_BANK_IFSC_CODE);
    }
    else {
      this.displayIfscCode = true;
      this.f.ifscCode.setValue('');
    }
  }

  mask(sensitive : string | undefined) : string {
    let format : string = 'XXXX XXXX XXXX';
    let maskedValue : string = '';
    if (sensitive != null && sensitive.length > 6) {
        let actualLength = sensitive.length;
        maskedValue = format.slice(0, actualLength - 2) + sensitive.slice(actualLength-2, actualLength) ;
    }
    return maskedValue;
  }
}
