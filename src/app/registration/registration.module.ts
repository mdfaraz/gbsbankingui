import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateRegistrationComponent } from './create-registration/create-registration.component';
import { RegistrationListComponent } from './registration-list/registration-list.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import { BeneficiaryListComponent } from './beneficiary-list/beneficiary-list.component';
import { CreateBeneficiaryComponent } from './create-beneficiary/create-beneficiary.component';
import { RegistrationTemplateComponent } from './registration-template/registration-template.component';
import { RegistrationLandingComponent } from './registration-landing/registration-landing.component';
import { MatStepperModule } from '@angular/material/stepper';
import { RegistrationStepsComponent } from './registration-steps/registration-steps.component';
import { AckComponent } from './ack/ack.component';
import { AddBeneficiaryComponent } from './add-beneficiary/add-beneficiary.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';


@NgModule({
  declarations: [
    CreateRegistrationComponent,
    RegistrationListComponent,
    BeneficiaryListComponent,
    CreateBeneficiaryComponent,
    RegistrationTemplateComponent,
    RegistrationLandingComponent,
    RegistrationStepsComponent,
    AckComponent,
    AddBeneficiaryComponent
  ],
  imports: [
    MatFormFieldModule,
    MatInputModule,
    CommonModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatGridListModule,
    MatTableModule,
    MatSelectModule,
    MatIconModule,
    MatStepperModule,
    MatSlideToggleModule
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    CreateRegistrationComponent,
    RegistrationListComponent,
    BeneficiaryListComponent,
    CreateBeneficiaryComponent
  ]
})
export class RegistrationModule { }
