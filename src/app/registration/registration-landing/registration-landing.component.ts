import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Constants } from 'src/app/core/common/models/constants';
import { ContactUsPopupComponent } from 'src/app/core/contact-us-popup/contact-us-popup.component';
import { HelpDialogBoxComponent } from 'src/app/core/help-dialog-box/help-dialog-box.component';

@Component({
  selector: 'app-registration-landing',
  templateUrl: './registration-landing.component.html',
  styleUrls: ['./registration-landing.component.css']
})
export class RegistrationLandingComponent implements OnInit {

  constructor(private router : Router,
    private dialog : MatDialog) { }

  ngOnInit(): void {
  }

  public registerExistingAccount() : void {
     this.router.navigate(['registration-template']);
  }

  public openNewAccount() : void {
    this.openHelpDialog();
  }

  openHelpDialog(): void {

    const dRef = this.dialog.open(ContactUsPopupComponent, {
      width: '60%'
    });
    
  }

}
