import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Constants } from 'src/app/core/common/models/constants';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';
import { Beneficiary } from '../models/beneficiary';
import { BeneficiaryService } from '../services/beneficiary.service';

@Component({
  selector: 'app-beneficiary-list',
  templateUrl: './beneficiary-list.component.html',
  styleUrls: ['./beneficiary-list.component.css']
})
export class BeneficiaryListComponent implements OnInit {

  public beneficiaryList: Array<Beneficiary> = new Array<Beneficiary>();
  private rowSelection;
  private appType: string = Constants.APP_TYPE;
  public appUserId : string = this.dataSharingService.getAppUserId();
  public appName : string = this.dataSharingService.getAppName();
  public userId : string = '';

  displayedColumns: string[] = [
    'name',
    'accountNumber',
    'bankName',
    'ifscCode',
    'userId',
    'createdDate',
    'active',
    'action'];

  public dataSource: Beneficiary[] = new Array<Beneficiary>();

  constructor(private beneficiaryService: BeneficiaryService,
              private dataSharingService : DataSharingService,
              private dialog : MatDialog,
              private router: Router) 
  {
    this.rowSelection = "single";
  }

  ngOnInit(): void {
      this.loadBeneficiaries();
  }

  isStatusEnabled(beneficiary :Beneficiary) : boolean {
    return beneficiary.statusEnabled ? true : false;
  }

  isActive(beneficiary :Beneficiary) : boolean {
    return beneficiary.active ? true : false;
  }

  deactivateBeneficiary(beneficiary :Beneficiary) : void {
      this.openDeleteDialog(beneficiary);
  }

  changeBeneficiaryStatus(beneficiary :Beneficiary, enable : boolean) : void {
      this.openChangeStatusDialog(beneficiary, enable)
  }

  addBeneficiary() : void {
    this.router.navigate(['/create-beneficiary']);
  }

  loadBeneficiaries() : void {
    this.beneficiaryService.getAllBeneficiaries(this.appName, 
      this.appUserId).subscribe(data => {
          this.beneficiaryList = data;
          console.log(this.beneficiaryList);

          this.dataSource = this.beneficiaryList;
      });
  }

  openDeleteDialog(beneficiary :Beneficiary): void {
    const dRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: {message:"Do you confirm the deletion of this beneficiary? once confirmed it will not be changed"}
    });
    dRef.afterClosed().subscribe(result => {
       if(result.data){
        console.log('Yes clicked');
        this.beneficiaryService.removeBeneficiary(beneficiary).subscribe(data => {
          let error = data["error"];
          if (error != undefined) {
            console.log(error);
          } else {
            console.log("removed beneficiary ...");
            this.loadBeneficiaries();
          }
        });
      }
    });
  }

  openChangeStatusDialog(beneficiary :Beneficiary, status : boolean): void {
    let enable = status ? 'enable' : 'disable';
    const dRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: {message:"Do you confirm ! You would like to "+enable+" this beneficiary ? You can change your choice later"}
    });
    dRef.afterClosed().subscribe(result => {
       if(result.data){
        console.log('Yes clicked');
        this.beneficiaryService.changeBeneficiaryStatus(beneficiary, status).subscribe(data => {
          let error = data["error"];
          if (error != undefined) {
            console.log(error);
          } else {
            console.log("updated beneficiary ..."+ status);
            this.loadBeneficiaries();
          }
        });
      }
    });
  }

  mask(sensitive : string | undefined) : string {
    let format : string = 'XXXX XXXX XXXX';
    let maskedValue : string = '';
    if (sensitive != null && sensitive.length > 6) {
        let actualLength = sensitive.length;
        maskedValue = format.slice(0, actualLength - 2) + sensitive.slice(actualLength-2, actualLength) ;
    }
    return maskedValue;
  }

}
