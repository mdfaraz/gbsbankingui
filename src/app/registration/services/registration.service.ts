import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';
import { UUID } from 'angular2-uuid';
import { Crypto } from 'src/app/core/common/services/crypto';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  private baseUrl = environment.baseUrl+'/api/v1/register';

  constructor(private http : HttpClient) { }

  registerUser(user: User): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(user))
    }; 
    console.log(headers);  
    return this.http.post(this.baseUrl + `/user`, user, {headers});
  }

  getRegistrationStatus(user : User): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(user))
    };
    return this.http.post(this.baseUrl + `/user/status`, user,{headers});
  }

  deregisterUser(user: User): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(user))
    };
    return this.http.post(this.baseUrl + `/user/delete`, user, {headers});
  }

  getAllRegistrations(appName:string, appUserId:string):  Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID()
    };
    let currentTimestamp  = Date.now();
    let queryParams = "?ts="+currentTimestamp;
    console.log(httpOptions);
    console.log(headers);
    return this.http.get(this.baseUrl + `/application/`+appName+"/"+appUserId+queryParams,  {headers});
  }

  setPrimaryAccount(user: User) : Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(user))
    }; 
    console.log(httpOptions);  
    return this.http.post(this.baseUrl + `/user/primaryAccount`, user, {headers});
  }
}
