import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Beneficiary } from '../models/beneficiary';

@Injectable({
  providedIn: 'root'
})
export class BeneficiaryService {

  private baseUrl = environment.baseUrl+'/api/v1/beneficiary';

  constructor(private http : HttpClient) { }

  addBeneficiary(beneficiary: Beneficiary): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID()
    };  
    return this.http.post(this.baseUrl + `/user`, beneficiary, {headers});
  }

  changeBeneficiaryStatus(beneficiary: Beneficiary, enable : boolean): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID()
    };
    return this.http.post(this.baseUrl + `/user/status/`+enable, beneficiary, {headers});
  }

  removeBeneficiary(beneficiary: Beneficiary): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID()
    };
    return this.http.post(this.baseUrl + `/user/delete`, beneficiary, {headers});
  }

  getUserBeneficiaries(appName:string, appUserId:string, corpId: string, userId: string):  Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID()
    };

    let currentTimestamp  = Date.now();
    let queryParams = "?ts="+currentTimestamp; 
    return this.http.get(this.baseUrl + `/users/`+appName+"/"+appUserId+"/"+corpId+"/"+userId+queryParams,  {headers});
  }

  getAllBeneficiaries(appName:string, appUserId:string):  Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID()
    };
    let currentTimestamp  = Date.now();
    let queryParams = "?ts="+currentTimestamp;
    return this.http.get(this.baseUrl + `/users/`+appName+"/"+appUserId+queryParams,  {headers});
  }
}
