import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration-steps',
  templateUrl: './registration-steps.component.html',
  styleUrls: ['./registration-steps.component.css']
})
export class RegistrationStepsComponent implements OnInit {

  constructor(private router :Router) { }

  ngOnInit(): void {
  }

  markDone() : void {
      this.router.navigate(['/register-list'])
  }

}
