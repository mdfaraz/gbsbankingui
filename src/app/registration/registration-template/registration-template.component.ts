import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { PartnerConfig } from 'src/app/core/common/models/partner-config';
import { User } from '../models/user';
import { UserStatus } from '../models/user-status';

@Component({
  selector: 'app-registration-template',
  templateUrl: './registration-template.component.html',
  styleUrls: ['./registration-template.component.css']
})
export class RegistrationTemplateComponent implements OnInit {

  isLinear = false;
  createRegistrationInfo: FormGroup;
  registeredUserInfo: FormGroup;
  stepsInfo : FormGroup;

  public errorMessage : string = '';
  public isError : boolean = false;

  constructor(private fb: FormBuilder) {
    this.createRegistrationInfo = this.fb.group({});
    this.registeredUserInfo = this.fb.group({});
    this.stepsInfo = this.fb.group({});
   }

   ngOnInit() : void {
      
   }

   public userStatus : User = new User();
   createRegistration(createUserStatus : User, stepper : MatStepper) : void {
    this.userStatus = createUserStatus; 
    stepper.next();
   }

   acknowledgeCreateUser(createUserStatus : User, stepper : MatStepper) : void {
    this.userStatus = createUserStatus; 
    stepper.next();
   }

}
