import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user';

@Component({
  selector: 'app-ack',
  templateUrl: './ack.component.html',
  styleUrls: ['./ack.component.css']
})
export class AckComponent implements OnInit {

  constructor(private router : Router) { }

  @Input() createdUserStatus : User = new User();
  @Output() acknowledgedEmitter = new EventEmitter<User>();
  
  ngOnInit(): void {
    console.log(this.createdUserStatus);
  }

  mask(sensitive : string | undefined) : string {
    let format : string = 'XXXX XXXX XXXX';
    let maskedValue : string = '';
    if (sensitive != null && sensitive.length > 6) {
        let actualLength = sensitive.length;
        maskedValue = format.slice(0, actualLength - 2) + sensitive.slice(actualLength-2, actualLength) ;
    }
    return maskedValue;
  }

  goToStepsScreen() : void {
      this.acknowledgedEmitter.emit(this.createdUserStatus);
  }

  goToRegisteredList() : void {
    this.router.navigate(['/register-list'])
  }

}
