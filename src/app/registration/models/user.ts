export class User {
    userId? : string;
    partnerId? : number;
    appUserId? : string;
    appType? : string;
    appName? : string;
    accountNumber? : string;
    corpId? : string;
    userName? : string;
    aliasId? : string;
    primaryAccount? : boolean;
}

class UserJson {

    getMessage(user : User) : string {
        /*let map : Map<string, string> = new Map<string, string>();
        map.set('userId', user.userId!)
        map.set('corpId', user.corpId!)
        map.set('aliasId', user.aliasId!)
        map.set('accountNumber', user.accountNumber!)
        map.set('appUserId', user.appUserId!)
        map.set('appName', user.appName!)*/

        let str = JSON.stringify(user, Object.keys(user).sort());

        return str;
    }

}