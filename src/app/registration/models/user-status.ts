export class UserStatus {
    userId? : string;
    appUserId? : string;
    appType? : string;
    appName? : string;
    partnerId? : number;
    corpId? : string;
    aliasId? : string;
    accountNumber? : string;
    currentState?: string;
    registrationTs? : Date;
    urnId? : string;
    updateTs? : Date;
    active? : boolean;
    primaryAccount? : boolean;
}
