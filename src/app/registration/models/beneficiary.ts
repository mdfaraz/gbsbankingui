export class Beneficiary {
    appUserId? : string;
    appName? : string;
    corpId? : string;
    userId? : string;
    name? : string;
    ifscCode? : string;
    accountNumber? : string;
    bankName? : string;
    withinSameBank? : boolean;
    active? : boolean;
    statusEnabled? : boolean;
}
