import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PartnerConfig } from 'src/app/core/common/models/partner-config';
import { ConfigService } from 'src/app/core/common/services/config.service';
import { User } from '../models/user';
import { RegistrationService } from '../services/registration.service';
import { Constants } from 'src/app/core/common/models/constants';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { MatDialog } from '@angular/material/dialog';
import { HelpDialogBoxComponent } from 'src/app/core/help-dialog-box/help-dialog-box.component';

@Component({
  selector: 'app-create-registration',
  templateUrl: './create-registration.component.html',
  styleUrls: ['./create-registration.component.css']
})
export class CreateRegistrationComponent implements OnInit {

  public createrRegisterForm = new FormGroup({
    userId: new FormControl('', [Validators.required]),
    partnerId: new FormControl('', [Validators.required]),
    corpId: new FormControl('', [Validators.required]),
    accountNumber: new FormControl('', [Validators.required, Validators.minLength(12)]),
    cnfAccountNumber : new FormControl('', [Validators.required, Validators.minLength(12)]),
    aliasId: new FormControl('')
  });

  public user: User = new User();
  public partners: Array<PartnerConfig> = new Array<PartnerConfig>();
  private appType: string = Constants.APP_TYPE;
  public appUserId : string = this.dataSharingService.getAppUserId();
  public appName : string = this.dataSharingService.getAppName();

  public partnerMap: Map<number, PartnerConfig> = new Map<number, PartnerConfig>(); 
  public selectedPartner : PartnerConfig = new PartnerConfig();
  public hide = true;

  public isError : boolean = false;
  public errorMessage : string = '';

  constructor(private registrationService: RegistrationService,
    private configService: ConfigService,
    private dataSharingService : DataSharingService,
    private dialog : MatDialog,
    private route : Router) {

  }

  get f() { return this.createrRegisterForm.controls; }

  ngOnInit(): void {
    this.configService.listPartner().subscribe(data => {
      console.log(data);
      this.partners = data;
      this.partners.forEach(e => {
        this.partnerMap.set(e.partnerId!, e);
      });
    });
  }

  @Output() userEmitter = new EventEmitter<User>();
  onSubmit() {
    this.isError = false;
    this.errorMessage = '';

    console.log(this.createrRegisterForm)
    if (this.createrRegisterForm.invalid) {

      let textMessage = '';
      const controls = this.f;
      for (const name in controls) {
          if (controls[name].invalid) {
            textMessage = "Please enter valid value for : " + name;
            break;
          }
      }

      this.errorMessage = textMessage;
      this.isError = true;
      return;
    }

    if (this.f.accountNumber.value != this.f.cnfAccountNumber.value) {
      this.errorMessage = "Mismatch in account number";
      this.isError = true;
      return;
    }

    this.user.userId = this.f.userId.value;
    this.user.aliasId = this.f.aliasId.value;
    this.user.corpId = this.f.corpId.value;
    this.user.partnerId = this.f.partnerId.value;
    this.user.accountNumber = this.f.accountNumber.value;
    this.user.appUserId = this.appUserId;
    this.user.appType = this.appType;
    this.user.appName = this.appName;
    this.user.primaryAccount = this.primaryAccountFlag;
    console.log(this.user);
  
    this.registrationService.registerUser(this.user).subscribe(data => {
      console.log(data);
      //this.route.navigate(['/register-list']);
      let error = data["error"];
      if (error == undefined) {
        this.userEmitter.emit(this.user);
      } else {
        this.errorMessage = error.errorMessage;
        this.isError = true;
      }
    })
  }

  public displayICICIBank : boolean = false;
  onPartnerChange(event : any) : void {
    var partnerId = event.value;
    if (partnerId != '') {
      this.selectedPartner = this.partnerMap.get(partnerId)!;
      if(this.selectedPartner.partnerName == 'ICICI bank') {
        this.displayICICIBank = true;
      }
    } else {
      this.selectedPartner = new PartnerConfig();
      this.displayICICIBank = false;
    }
  }

  openHelpDialog(diaglogMessagekey : string): void {

    let diaglogMessage = Constants.dialogMessgeMap().get(diaglogMessagekey);

    const dRef = this.dialog.open(HelpDialogBoxComponent, {
      width: '350px',
      data: {message:diaglogMessage}
    });
    
  }

  public primaryAccountFlag : boolean = false;
  togglePrimaryAccount() : void {
      this.primaryAccountFlag = !this.primaryAccountFlag;
  }

}
