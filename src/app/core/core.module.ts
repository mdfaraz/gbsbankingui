import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { ButtonComponent } from './button/button.component';
import { NavComponent } from './nav/nav.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import { RouterModule } from '@angular/router';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import { TemplateComponent } from './template/template.component';
import { LogoutComponent } from './logout/logout.component';
import { AccountCardComponent } from './account-card/account-card.component';
import { HelpDialogBoxComponent } from './help-dialog-box/help-dialog-box.component';
import { CallbackComponent } from './callback/callback.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ContactUsPopupComponent } from './contact-us-popup/contact-us-popup.component';
import { MatButtonModule } from '@angular/material/button';
import { DisableRightClickDirective } from './guard/disable-right-click.directive';
import { DisableCopyPasteDirective } from './directives/disable-copy-paste.directive';

@NgModule({
  declarations: [
    HeaderComponent,
    ButtonComponent,
    NavComponent,
    TemplateComponent,
    LogoutComponent,
    AccountCardComponent,
    HelpDialogBoxComponent,
    CallbackComponent,
    ContactUsComponent,
    ConfirmationDialogComponent,
    ContactUsPopupComponent,
    DisableRightClickDirective,
    DisableCopyPasteDirective
  ],
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    CommonModule,
    RouterModule,
    MatSidenavModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatProgressSpinnerModule
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    HeaderComponent,
    ButtonComponent,
    NavComponent,
    TemplateComponent,
    LogoutComponent,
    AccountCardComponent,
    HelpDialogBoxComponent,
    CallbackComponent,
    ContactUsComponent,
    ContactUsPopupComponent
  ]
})
export class CoreModule { }
