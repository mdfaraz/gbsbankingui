import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Constants } from '../common/models/constants';
import { DataSharingService } from '../common/services/data-sharing.service';
import { HelpDialogBoxComponent } from '../help-dialog-box/help-dialog-box.component';
import { Contact } from './contact';
import { ContactService } from './contact.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  public contactUsForm = new FormGroup({
    organaizationName: new FormControl('', [Validators.required]), 
    organizationType:  new FormControl('', [Validators.required]), 
    organizationSize: new FormControl('', [Validators.required]), 
    organizationSegment : new FormControl('', [Validators.required]), 
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    mobileNumber: new FormControl('+91', [Validators.required,
          Validators.minLength(10),
          Validators.maxLength(13)]),
    emailId: new FormControl('', [Validators.required, Validators.email]),
    message : new FormControl('')
  });

  public isError : boolean = false;
  public errorMessage : string = '';

  private appType: string = Constants.APP_TYPE;
  public appUserId : string = this.dataSharingService.getAppUserId();
  public appName : string = this.dataSharingService.getAppName();

  private contact : Contact = new Contact();

  constructor(private contactService : ContactService,
    private dataSharingService : DataSharingService,
    private dialog : MatDialog,
    private route : Router) { }

  ngOnInit(): void {
  }

  get f() { return this.contactUsForm.controls; }

  onSubmit() : void {

    this.isError = false;
    this.errorMessage = '';

    console.log(this.contactUsForm)
    if (this.contactUsForm.invalid) {
      let textMessage = '';
      const controls = this.f;
        for (const name in controls) {
            if (controls[name].invalid) {
              textMessage = "Please enter valid value for : " + name;
              break;
            }
        }

      this.errorMessage = textMessage;
      this.isError = true;
      return;
    }

    this.contact.appUserId = this.appUserId;
    this.contact.appName = this.appName;
    this.contact.appType = this.appType;
    this.contact.contactNumber = this.f.mobileNumber.value;
    this.contact.organization = this.f.organaizationName.value;
    this.contact.organizationType = this.f.organizationType.value;
    this.contact.organizationSize = this.f.organizationSize.value;
    this.contact.organizationSegment = this.f.organizationSegment.value;
    this.contact.firstName = this.f.firstName.value;
    this.contact.lastName = this.f.lastName.value;
    this.contact.emailId = this.f.emailId.value;
    this.contact.message = this.f.message.value;

    this.contactService.addContact(this.contact).subscribe(data => {
      let error = data["error"];
      if (error != undefined) {
        this.errorMessage = error.errorMessage;
        this.isError = true;
      } else {
        //this.route.navigate(['/statement']);
        this.openHelpDialog('thank_you_confirm_id');
      }
    })

  }

  openHelpDialog(diaglogMessagekey : string): void {

    let diaglogMessage = Constants.dialogMessgeMap().get(diaglogMessagekey);

    const dRef = this.dialog.open(HelpDialogBoxComponent, {
      width: '350px',
      data: {message:diaglogMessage}
    });

    this.contactUsForm = new FormGroup({
      organaizationName: new FormControl('', [Validators.required]), 
      organizationType:  new FormControl('', [Validators.required]), 
      organizationSize: new FormControl('', [Validators.required]), 
      organizationSegment : new FormControl('', [Validators.required]), 
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      mobileNumber: new FormControl('', [Validators.required]),
      emailId: new FormControl('', [Validators.required]),
      message : new FormControl('')
    });
    
  }
  

}
