export class Contact {
    appUserId? : string;
    appName? : string;
    appType? : string;
    firstName? : string;
    lastName? : string;
    organization? : string;
    organizationType? : string;
    organizationSize? : string;
    organizationSegment? : string;
    contactNumber? : string;
    emailId? : string;
    message? : string;
}