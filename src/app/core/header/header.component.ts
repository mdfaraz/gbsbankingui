import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { DataSharingService } from '../common/services/data-sharing.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  public username : string = this.dataSharingService.getAppUserId();
  public appName : string = this.dataSharingService.getAppName();

  constructor(private dataSharingService : DataSharingService,
    private router : Router) { 
    }

  ngOnInit(): void {
    
  }


  logout() : void {
      this.dataSharingService.clearLocalStorage();
      this.router.navigate(['costipro']);
  }

  callback() : void {
    this.dataSharingService.clearLocalStorage();
    this.router.navigate(['costipro']);
  }


}
