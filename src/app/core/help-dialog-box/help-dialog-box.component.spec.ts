import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpDialogBoxComponent } from './help-dialog-box.component';

describe('HelpDialogBoxComponent', () => {
  let component: HelpDialogBoxComponent;
  let fixture: ComponentFixture<HelpDialogBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HelpDialogBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpDialogBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
