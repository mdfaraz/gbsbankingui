import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface HelpMessage {
  header : string;
  message: string;
}

@Component({
  selector: 'app-help-dialog-box',
  templateUrl: './help-dialog-box.component.html',
  styleUrls: ['./help-dialog-box.component.css']
})
export class HelpDialogBoxComponent implements OnInit {

  ngOnInit() {
  }

  constructor(public dialogRef: MatDialogRef<HelpDialogBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: HelpMessage) { }

  public back(): void {
    this.dialogRef.close();
  }

  public close() : void {
    this.dialogRef.close();
  }

}
