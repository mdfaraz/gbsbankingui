import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DataSharingService } from '../common/services/data-sharing.service';

interface ROUTE {
  icon?: string;
  route?: string;
  title?: string;
}

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public isLoggedIn$: Observable<boolean> = new Observable<boolean>();

  constructor(private dataSharingService : DataSharingService,
            private router : Router) { 
    }

  ngOnInit(): void {
    this.isLoggedIn$ = this.dataSharingService.isLoggedIn;
  }

  myWorkRoutes: ROUTE[] = [
    {
      icon: 'home',
      route: 'dashboard',
      title: 'Dashboard'
    },
    {
      icon: 'manage_accounts',
      route: 'register-list',
      title: 'Manage Registration'
    },
    {
      icon: 'person_add_alt_1',
      route: 'contact-us',
      title: 'New Account'
    },
    {
      icon: 'payments',
      route: 'txn-train',
      title: 'Initiate Transaction'
    },
    {
      icon: 'people',
      route: 'manage_beneficiary',
      title: 'Manage Beneficiary'
    },
    {
      icon: 'account_balance',
      route: 'statement',
      title: 'Statement'
    },
    {
      icon: 'credit_score',
      route: 'contact-us',
      title: 'OD Limits'
    },
    {
      icon: 'exit_to_app',
      route: 'logout',
      title: 'Sign out'
    }
  ];

  public isUserLoggedIn() {
    return this.dataSharingService.isLoggedIn;
  }

  callback() : void {
    this.router.navigate(['costipro']);
  }

}
