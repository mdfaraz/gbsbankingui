import { Component, Input, OnInit } from '@angular/core';
import { Constants } from '../common/models/constants';
import { AccountCard } from './models/account-card';

@Component({
  selector: 'app-account-card',
  templateUrl: './account-card.component.html',
  styleUrls: ['./account-card.component.css']
})
export class AccountCardComponent implements OnInit {


  @Input() public accountCard : AccountCard = new AccountCard();

  public currencySymbolMap : Map<string, string> = Constants.currencySymbolMap();

  constructor() { }

  ngOnInit(): void {
    
  }

  mask(sensitive : string | undefined) : string {
    let format : string = 'XXXX XXXX XXXX';
    let maskedValue : string = '';
    if (sensitive != null && sensitive.length > 6) {
        let actualLength = sensitive.length;
        maskedValue = format.slice(0, actualLength - 2) + sensitive.slice(actualLength-2, actualLength) ;
    }
    return maskedValue;
  }



}
