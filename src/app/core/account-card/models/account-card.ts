export class AccountCard {
    cardLogoPath? : string;
    cardColor? : string;
    accountNumber? : string;
    accountBalance? : number;
    bankName? : string;
    currency? : string;
}
