import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  private redirectURL = environment.costiproURL;
 
  constructor() { }

  ngOnInit(): void {
    window.location.href = this.redirectURL;
    window.close();
  }

}
