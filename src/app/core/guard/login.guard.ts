import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { DataSharingService } from '../common/services/data-sharing.service';


@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  
  public isUserVerified : boolean = false;

  constructor(private dataSharingService : DataSharingService,
    private bnIdle: BnNgIdleService,
    private router : Router) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree 
    {
      /*
      if (this.dataSharingService.loggedIn.pipe()) {
        return true;
      } else {
        this.router.navigate(['costipro']);
        return false;
      }
      */
     return this.dataSharingService
                .loggedIn
                .pipe(take(1), 
                    map((isLoggedIn: boolean) => {
                      if (!isLoggedIn) {
                        //this.router.navigate(['/verifyPin']);
                        this.router.navigate(['costipro']); // logout
                        return false;
                      }
                      return true;
                    }));
  }
  
}
