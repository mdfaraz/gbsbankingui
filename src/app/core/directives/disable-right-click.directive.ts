import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appDisableRightClick]'
})
export class DisableRightClickDirective {

  @HostListener('contextmenu', ['$event'])
  onRightClick(event : Event) {
    event.preventDefault();
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event: Event) {
    console.log('Back button pressed');
    event.preventDefault();
  }

  constructor() { }
}
