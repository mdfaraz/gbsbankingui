export class HelpRenderMapper {

    public static help_corporate_id : string = '<div class="row"><div class="col-12"><b>Coporate ID</b> <br><ul><li>You may check under <b>My Profile</b> option in Internet banking - User the same format</li></ul></div></div>'; 

    public static help_user_id : string = '<div class="row"><div class="col-12"><b>User ID</b> <br><ul><li>You may check under <b>My Profile</b> option in internet banking - Use the same format <br> e.g if it is in Capital letters then use capital letters only</li></ul></div></div>'; 

    public static help_account_number : string = '<div class="row"><div class="col-12"><b>Account Numner</b> <br><ul><li>Account Number as provided in net banking login <br> or may also check in welcome kit</li></ul></div></div>'; 

    public static help_banklogin_id : string = '<div class="row"><div class="col-12"><b>Bank Login ID (Alias ID)</b> <br><ul><li>Bank login Id is a Alias ID, As provided in net banking login <br> or may also check in welcome kit</li></ul></div></div>'; 

    public static thank_you_confirm_id : string = '<div class="row"><div class="col-12"><b>Thank You !</b> <br><ul><li>We have captured the records <br> We are contact you shortly.</li></ul></div></div>'; 

    public static timeout_exception_text : string = 'Response is received from the bank due to delay in message from the banking server. Kindly check in the statement - '+
                            'if the initiated transaction is being processed/ successfully completed. If not – then you may initiate again, in case of transaction is part of statement - '+
                            ' then amount is credited to Beneficiary/ Payee account <br>' +
                            'For any detail query/ information – You may write an email on corporatecare@icicibank.com from your registered e-mail ID or call on ICICI Bank Corporate Customer Care';

}
