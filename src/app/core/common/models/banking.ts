export class BankingRequest {
    appUserId? : string;
    appName? : string;
    approvalAmount? : number;
    expenseTitle? : string;
    transactionId? : string;
    status? : string;
}