import { HelpRenderMapper } from "./help-render-mapper";

export class Constants {
   
    public static APP_NAME='COSTIPRO';
    public static APP_TYPE = 'WEB';
    public static ICICI_BANK_IFSC_CODE = 'ICIC0000011';
    public static ICICI_BANK_INTRA_BANK_TRANSFER = 'TPA';
    public static REFERENCE_ID_PREFIX = 'CUBP';

    public static IDLE_TIMEOUT = 15 * 60;

    public static SECRET_CODE = 'Q09TVFJQUk9AMjAyMg=='; //"COSTRPRO@2022"
    public static SIGNATURE_SECRET_CODE = 'secret-key-12345';

    public static PARTNER_ICICI = 'ICICI';

    public static statusMap(): Map<string, string> {
        let statusMap : Map<string, string> = new Map<string, string>();
        statusMap.set('REGISTERED', 'Registered');
        statusMap.set('DEREGISTERED', 'De-registered');
        statusMap.set('PENDING_SELF_APPROVAL', 'Pending for Approval');
        return statusMap;
    }

    public static transactionTypeMap() : Map<string, string> {
        let txTypeMap : Map<string, string> = new Map<string, string>();
        txTypeMap.set('TPA', 'Within ICICI bank');
        txTypeMap.set('IFS', 'IMPS');
        txTypeMap.set('RTG', 'RTGS');
        txTypeMap.set('RGS', 'NEFT');
        return txTypeMap;
      }

    public static currencySymbolMap(): Map<string, string> {
        let currencySymbolMap : Map<string, string> = new Map<string, string>();
        currencySymbolMap.set('INR', '₹');
        return currencySymbolMap;
    }  

    public static typeMap(): Map<string, string> {
        let typeMap : Map<string, string> = new Map<string, string>();
        typeMap.set('CR', 'Credit');
        typeMap.set('DR','Debit');
        return typeMap;
    }

    public static dialogMessgeMap() : Map<string, string> {
        let dialogMessgeMap : Map<string, string> = new Map<string, string>();
        dialogMessgeMap.set('help_corporate_id', HelpRenderMapper.help_corporate_id);
        dialogMessgeMap.set('help_user_id', HelpRenderMapper.help_user_id);
        dialogMessgeMap.set('help_account_number', HelpRenderMapper.help_account_number);
        dialogMessgeMap.set('help_banklogin_id', HelpRenderMapper.help_banklogin_id);
        dialogMessgeMap.set('thank_you_confirm_id', HelpRenderMapper.thank_you_confirm_id);
        return dialogMessgeMap;
    }

    public static getRandomString(length: Number) : string {
        var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var result = '';
        for ( var i = 0; i < length; i++ ) {
            result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
        }
        return result;
    }

    public static formatDate() : string {
        let date = new Date();
        var year = date.getFullYear().toString();
        var month = (date.getMonth() + 101).toString().substring(1);
        var day = (date.getDate() + 100).toString().substring(1);
        return year + month + day;
    }

    public static displayErrorMessage() : Map<string, string> {
        let dialogMessgeMap : Map<string, string> = new Map<string, string>();
        dialogMessgeMap.set('transaction_timeout', HelpRenderMapper.timeout_exception_text);
        return dialogMessgeMap;
    }
}
