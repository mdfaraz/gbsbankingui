export class Error {
    error? : ErrorMsg;
}

export class ErrorMsg {
    errorCode? : string;
    errorMessage? : string;
}
