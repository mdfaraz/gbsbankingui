export class PartnerConfig {
    partnerId? : number;
    partnerName? : string;
    partnerDetail? : string;
    country? : string;
    currency? : string;
    partnerDisplayName? : string;
    partnerLogoLocation? : string;
    partnerThemeColor? : string;
}
