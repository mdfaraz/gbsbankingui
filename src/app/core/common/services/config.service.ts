import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PartnerConfig } from '../models/partner-config';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private baseUrl = environment.baseUrl+'/api/v1/config';

  constructor(private http:HttpClient) { }

  listPartner(): Observable<any> {   
    return this.http.get(this.baseUrl + `/partners`, httpOptions);
  }

  getPartner(partnerId: number): Observable<any> {   
    return this.http.get(this.baseUrl + `/partner/`+partnerId, httpOptions);
  }

}
