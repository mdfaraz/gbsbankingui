import { sha256 } from 'js-sha256';
import { Constants } from '../models/constants';
import * as CryptoJS from 'crypto-js';

export class Crypto {

    public static getSignature(message : string) : string {
        var hash = sha256.hmac.create(Constants.SECRET_CODE);
        hash.update(message);
        return hash.hex();
    }
/*
    public static getSignature(message : string) : string {
        //var utf8Message = CryptoJS.enc.Utf8.parse(message);
        let utf8Encode = new TextEncoder();
        utf8Encode.encode(message);

        //message.

        //var utf8str = CryptoJS.enc.Utf8.stringify(message);
        var signature = CryptoJS.AES
                        .encrypt(message, Constants.SIGNATURE_SECRET_CODE)
                        .toString();
        //var base64EncodedSignature = CryptoJS.enc.Base64.stringify(signature);
        //var utf8Message = CryptoJS.enc.Utf8.parse(signature);
        //var utf8str = CryptoJS.enc.Utf8.stringify(utf8Message);
        console.log("generated signature "+signature);
        return signature;
    } */

    public static getMessage(obj : any) : string {
        let str = JSON.stringify(obj, Object.keys(obj).sort());
        console.log(str);
        return str;
    }
}
