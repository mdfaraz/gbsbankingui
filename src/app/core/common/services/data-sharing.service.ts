import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {

  public loggedIn : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

  getAppUserId() : string {
    let storeAppUserId = localStorage.getItem('appUserId');
    if (storeAppUserId != null) {
      return storeAppUserId;
    }
    return '';
  }

  getAppName() : string {
    let storeAppUserName = localStorage.getItem('appName');
    if (storeAppUserName != null) {
      return storeAppUserName;
    }
    return '';
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  clearLocalStorage() : void {
    localStorage.clear();
  }

  initializeLoginUser(appUserId: string, appName : string): void {
    localStorage.setItem('appUserId', appUserId);
    localStorage.setItem('appName', appName);
  }

  updateUserLoggedIn() : void {
    this.loggedIn.next(true);
  }
}
