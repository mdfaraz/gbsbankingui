import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { RegistrationLandingComponent } from 'src/app/registration/registration-landing/registration-landing.component';
import { Constants } from '../common/models/constants';
import { DataSharingService } from '../common/services/data-sharing.service';
import { Contact } from '../contact-us/contact';
import { ContactService } from '../contact-us/contact.service';
import { HelpDialogBoxComponent } from '../help-dialog-box/help-dialog-box.component';

@Component({
  selector: 'app-contact-us-popup',
  templateUrl: './contact-us-popup.component.html',
  styleUrls: ['./contact-us-popup.component.css']
})
export class ContactUsPopupComponent implements OnInit {

  public contactUsForm = new FormGroup({
    organaizationName: new FormControl('', [Validators.required,
        Validators.maxLength(50)]), 
    organizationType:  new FormControl('', [Validators.required]), 
    organizationSize: new FormControl('', [Validators.required]), 
    organizationSegment : new FormControl('', [Validators.required]), 
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    mobileNumber: new FormControl('+91', [Validators.required,
          Validators.minLength(10),
          Validators.maxLength(13)]),
    emailId: new FormControl('', [Validators.required, Validators.email]),
    message : new FormControl('')
  });

  public isError : boolean = false;
  public errorMessage : string = '';

  private appType: string = Constants.APP_TYPE;
  public appUserId : string = this.dataSharingService.getAppUserId();
  public appName : string = this.dataSharingService.getAppName();

  private contact : Contact = new Contact();

  constructor(private contactService : ContactService,
    private dataSharingService : DataSharingService,
    public dialogRef: MatDialogRef<RegistrationLandingComponent>,
    private dialog : MatDialog,
    private route : Router) { }

  ngOnInit(): void {
  }

  get f() { return this.contactUsForm.controls; }

  onSubmit() : void {

    this.isError = false;
    this.errorMessage = '';

    console.log(this.contactUsForm)
    if (this.contactUsForm.invalid) {
      let textMessage = '';
      const controls = this.f;
        for (const name in controls) {
            if (controls[name].invalid) {
              textMessage = "Please enter valid value for : " + name;
            }
        }

      this.errorMessage = textMessage;
      this.isError = true;
      return;
    }

    this.contact.appUserId = this.appUserId;
    this.contact.appName = this.appName;
    this.contact.appType = this.appType;
    this.contact.contactNumber = this.f.mobileNumber.value;
    this.contact.organization = this.f.organaizationName.value;
    this.contact.organizationType = this.f.organizationType.value;
    this.contact.organizationSize = this.f.organizationSize.value;
    this.contact.organizationSegment = this.f.organizationSegment.value;
    this.contact.firstName = this.f.firstName.value;
    this.contact.lastName = this.f.lastName.value;
    this.contact.emailId = this.f.emailId.value;
    this.contact.message = this.f.message.value;

    this.contactService.addContact(this.contact).subscribe(data => {
      let error = data["error"];
      if (error != undefined) {
        this.errorMessage = error.errorMessage;
        this.isError = true;
      } else {
        //this.route.navigate(['/statement']);
        this.back();
        this.openHelpDialog('thank_you_confirm_id');
      }
    })

  } 

  back() {
    //this.route.navigate(['/home']);
    this.dialogRef.close();
  }

  openHelpDialog(diaglogMessagekey : string): void {

    let diaglogMessage = Constants.dialogMessgeMap().get(diaglogMessagekey);

    const dRef = this.dialog.open(HelpDialogBoxComponent, {
      width: '350px',
      data: {message:diaglogMessage}
    });

  }

}
