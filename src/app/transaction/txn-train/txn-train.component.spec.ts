import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TxnTrainComponent } from './txn-train.component';

describe('TxnTrainComponent', () => {
  let component: TxnTrainComponent;
  let fixture: ComponentFixture<TxnTrainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TxnTrainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TxnTrainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
