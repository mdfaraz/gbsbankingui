import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { BankingRequest } from 'src/app/core/common/models/banking';
import { PartnerConfig } from 'src/app/core/common/models/partner-config';
import { ConfigService } from 'src/app/core/common/services/config.service';
import { UserStatus } from 'src/app/registration/models/user-status';
import { RegistrationService } from 'src/app/registration/services/registration.service';
import { Transaction } from '../models/transaction';
import { TransactionService } from '../services/transaction.service';

@Component({
  selector: 'app-txn-train',
  templateUrl: './txn-train.component.html',
  styleUrls: ['./txn-train.component.css']
})
export class TxnTrainComponent implements OnInit {

  isLinear = false;
  generateOtpInfo: FormGroup;
  initateTransactionInfo: FormGroup;

  public partnerIdNameMap = new Map();
  public partners = new Array<PartnerConfig>();

  public userIdMap = new Map();
  public userStatusList: Array<UserStatus> = new Array<UserStatus>();
  public selectedUser : UserStatus = new UserStatus();
  public bankingRequest : BankingRequest = new BankingRequest();

  public errorMessage : string = '';
  public isError : boolean = false;

  public passedStepper : MatStepper | undefined;

  constructor(private fb: FormBuilder,
    private router: ActivatedRoute) {
    this.generateOtpInfo = this.fb.group({});
    this.initateTransactionInfo = this.fb.group({});
   }

   ngOnInit() : void {
      console.log(this.generateOtpInfo)
      console.log(this.initateTransactionInfo);

      this.router.queryParams.subscribe(params => { 
        let encodedStr = params['expenseItem'];
        if (encodedStr != undefined) {
          let decodedStr = atob(encodedStr);
          this.bankingRequest = JSON.parse(decodedStr);
        }
      });
   }

   get f() { 
     return this.generateOtpInfo.controls; 
    }

  public initiateTransaction : Transaction = new Transaction();
  generatedTransaction(generateOTPTransaction : Transaction, stepper : MatStepper) {
     this.initiateTransaction = generateOTPTransaction;
     if (this.bankingRequest != undefined && this.bankingRequest.transactionId != undefined) 
     {
      this.initiateTransaction.expenseApproval = true;
      this.initiateTransaction.expenseTransactionId = this.bankingRequest.transactionId;
     } 
     stepper.next();
     this.passedStepper = stepper;
  }

  public finalTransaction :Transaction = new Transaction();
  finalStatusStep(initiatedTransactionRef : Transaction,  stepper : MatStepper) {
    this.finalTransaction = initiatedTransactionRef; 
    stepper.next();
    this.passedStepper = stepper;
    this.resetStepperData();
  }

  public resetStepperData() : void {
    this.bankingRequest = new BankingRequest();
    this.generateOtpInfo = this.fb.group({});
    this.initateTransactionInfo = this.fb.group({});
  }

}
