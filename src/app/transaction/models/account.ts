export class Account {
    userId? : string;
    partnerId? : number;
    appUserId? : string;
    appType? : string;
    urn? : string;
    corpId? : string;
    accountNumber? : string;
    balance? : number;
    message? : string;
}
