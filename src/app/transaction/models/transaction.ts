export class Transaction {
    userId? : string;
    partnerId? : number;
    appUserId? : string;
    appType? : string;
    corpId? : string;
    urnId? : string;
    uniqueId? : string;
    debitAcc? : string;
    creditAcc? : string;
    ifscCode? : string;
    amount? : number;
    currency? : string;
    txType? : string;
    payeeName? : string;
    otpValue? : number;
    remarks? : string;
    status? : string;
    additionalInfo? : string;
    isExpenseCreationReq? : boolean;
    expenseApproval? : boolean;
    expenseTransactionId? : string;
    expenseStatus? : string;
}

export class EnquireTransaction {
    userId? : string;
    partnerId? : number;
    appUserId? : string;
    appType? : string;
    corpId? : string;
    urnId? : string;
    uniqueId? : string;
    userName? : string;
    aliasId? :string;
    additionalInfo? : string;
}
