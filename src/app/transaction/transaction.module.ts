import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormGroupDirective, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTabsModule} from '@angular/material/tabs';
import {MatStepperModule} from '@angular/material/stepper';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { GenerateOtpComponent } from './generate-otp/generate-otp.component';
import { CreateTransactionComponent } from './create-transaction/create-transaction.component';
import { TxnTrainComponent } from './txn-train/txn-train.component';
import { TransactionStatusComponent } from './transaction-status/transaction-status.component';
import { TxnAckComponent } from './txn-ack/txn-ack.component';

@NgModule({
  declarations: [
    GenerateOtpComponent,
    CreateTransactionComponent,
    TxnTrainComponent,
    TransactionStatusComponent,
    TxnAckComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    CommonModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatGridListModule,
    MatTabsModule,
    MatStepperModule,
    FormGroupDirective,
    MatProgressSpinnerModule
  ],
  exports: [
    CreateTransactionComponent,
    GenerateOtpComponent,
    TxnTrainComponent,
    TxnAckComponent
  ]
})
export class TransactionModule { }
