import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y/input-modality/input-modality-detector';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { ControlContainer, FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { Router } from '@angular/router';
import { Constants } from 'src/app/core/common/models/constants';
import { EnquireTransaction, Transaction } from '../models/transaction';
import { TransactionService } from '../services/transaction.service';

@Component({
  selector: 'app-create-transaction',
  templateUrl: './create-transaction.component.html',
  styleUrls: ['./create-transaction.component.css'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective }
  ]
})
export class CreateTransactionComponent implements OnChanges {

  public initiateTxnSubForm = new FormGroup({
    otpValue : new FormControl('', [Validators.required])
  });
  public form: FormGroup = new FormGroup({});
  @Input() public transactionRequest : Transaction = new Transaction();
  public transactionDtl : Transaction = new Transaction();

  @Output() finalTransactionEmitter = new EventEmitter<Transaction>();

  @Input() stepperData: MatStepper | undefined;

  public errorMessage : string = '';
  public isError : boolean = false;

  public txTypeMap : Map<string, string> = Constants.transactionTypeMap();
  public txtErrorMsg : Map<string, string> = Constants.displayErrorMessage();

  constructor(private fb : FormBuilder,
              private router : Router,
              private transactionService : TransactionService) { 
  }

  ngOnChanges(): void 
  {
    this.transactionDtl = this.transactionRequest;
  }

  get f() { return this.initiateTxnSubForm.controls; } 

  initateTransaction(): void {

    if (!this.isFormValid()) {
      return;
    }

    let otpValue = this.f.otpValue.value;
    this.transactionRequest.otpValue = parseInt(otpValue);
    //this.transactionRequest.isExpenseCreationReq = this.chkExpenseItem;
    console.log(this.transactionRequest);
    //this.finalTransaction = this.transactionRequest;
    this.transactionService.initiateTxn(this.transactionRequest)
    .subscribe(data => {
        console.log(data);

        let error = data["error"];
        if (error != undefined) {
          this.isError = true;
          this.errorMessage = error.errorCode + " = " + error.errorMessage;
          if (error.errorMessage.indexOf('timed out') > 0) 
          { 
            // display timeout message ??
            this.errorMessage = this.txtErrorMsg.get('transaction_timeout') != undefined ?
                                this.txtErrorMsg.get('transaction_timeout') :
                                error.errorMessage;
          }
        } else {
           let transactionResp : EnquireTransaction = data;
           let additionalInfo = transactionResp.additionalInfo;
           this.transactionRequest.additionalInfo = additionalInfo;
           this.finalTransactionEmitter.emit(this.transactionRequest);
           this.isError = false;
          this.errorMessage = '';
        }
    });
  }

  isFormValid(): boolean {
    if (this.initiateTxnSubForm.invalid) {
      let textMessage = '';
      const controls = this.f;
      for (const name in controls) {
          if (controls[name].invalid) {
            textMessage = "Please enter valid value for : " + name;
            break;
          }
      }

      this.errorMessage = textMessage;
      this.isError = true;
      return false;
    }

    this.isError = false;
    this.errorMessage = ''

    return true;
  }

  mask(sensitive : string | undefined) : string {
    let format : string = 'XXXX XXXX XXXX';
    let maskedValue : string = '';
    if (sensitive != null && sensitive.length > 6) {
        let actualLength = sensitive.length;
        maskedValue = format.slice(0, actualLength - 2) + sensitive.slice(actualLength-2, actualLength) ;
    }
    return maskedValue;
  }

  public cancelTransaction() : void {
    this.router.navigate(['statement']);
  }

  public back() : void {
    console.log(this.stepperData);
    if (this.stepperData != undefined) {
      this.stepperData.previous();
    }
  }

}
