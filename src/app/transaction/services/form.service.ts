import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { Transaction } from '../models/transaction';

@Injectable()
export class FormService {

  private stepOneSource: Subject<FormGroup> = new Subject();
  stepOne: Observable<FormGroup> = this.stepOneSource.asObservable();

  private stepTwoSource: Subject<FormGroup> = new Subject();
  stepTwo: Observable<FormGroup> = this.stepTwoSource.asObservable();

  private transactionRequest : Transaction = new Transaction();

  transactionMainForm: FormGroup = this._formBuilder.group({
    userId : '',
    debitAcc : '',
    creditAcc : '',
    ifscCode : '',
    amount : '',
    txType : '',
    payeeName : '',
    remarks : '',
    otpValue : ''
  })

  constructor(
    private _formBuilder: FormBuilder
  ) {
    this.stepOne.subscribe(form =>
      form.valueChanges.subscribe(val => {
        this.transactionMainForm.value.userId = val.userId
        this.transactionMainForm.value.debitAcc = val.debitAcc
        this.transactionMainForm.value.creditAcc = val.creditAcc
        this.transactionMainForm.value.ifscCode = val.ifscCode
        this.transactionMainForm.value.amount = val.amount
        this.transactionMainForm.value.txType = val.txType
        this.transactionMainForm.value.payeeName = val.payeeName
        this.transactionMainForm.value.remarks = val.remarks

        console.log(this.transactionMainForm);
      })
    )
    this.stepTwo.subscribe(form =>
      form.valueChanges.subscribe(val => {
        this.transactionMainForm.value.otpValue = val.otpValue
        console.log(this.transactionMainForm);
      })
    )
  }

  get txnForm() { return this.transactionMainForm.controls; } 

  stepReady(form: FormGroup, part: string) {
    switch (part) {
      //case 'generateOTP': { this.stepOneSource.next(form) }
      //case 'initiateTxn': { this.stepTwoSource.next(form) }
    }
  }
}