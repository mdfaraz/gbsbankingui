import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EnquireTransaction, Transaction } from '../models/transaction';
import { Crypto } from 'src/app/core/common/services/crypto';


@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  private baseUrl = environment.baseUrl+'/api/v1/transaction';

  constructor(private http : HttpClient) { }

  generateOTP(transaction: Transaction): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(transaction))
    };
    return this.http.post(this.baseUrl + `/generate/otp`, transaction, {headers});
  }

  initiateTxn(transaction: Transaction): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(transaction))
    };
    return this.http.post(this.baseUrl + `/initiate`, transaction, {headers});
  }

  enquireTxn(enquireTxn: EnquireTransaction): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(enquireTxn))
    };
    return this.http.post(this.baseUrl + `/enquire`, enquireTxn, {headers});
  }

}
