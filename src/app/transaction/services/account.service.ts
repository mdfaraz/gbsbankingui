import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Account } from '../models/account';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private baseUrl = environment.baseUrl+'/api/v1/account';

  constructor(private http : HttpClient) { }

  accountBalance(acountReq: Account): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID()
    };
    return this.http.post(this.baseUrl + `/balance`, acountReq, {headers});
  }

}
