import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y/input-modality/input-modality-detector';
import { utf8Encode } from '@angular/compiler/src/util';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ControlContainer, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UUID } from 'angular2-uuid';
import { AccountCard } from 'src/app/core/account-card/models/account-card';
import { BankingRequest } from 'src/app/core/common/models/banking';
import { Constants } from 'src/app/core/common/models/constants';
import { PartnerConfig } from 'src/app/core/common/models/partner-config';
import { ConfigService } from 'src/app/core/common/services/config.service';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { CreateBeneficiaryComponent } from 'src/app/registration/create-beneficiary/create-beneficiary.component';
import { Beneficiary } from 'src/app/registration/models/beneficiary';
import { UserStatus } from 'src/app/registration/models/user-status';
import { BeneficiaryService } from 'src/app/registration/services/beneficiary.service';
import { RegistrationService } from 'src/app/registration/services/registration.service';
import { Account } from '../models/account';
import { Transaction } from '../models/transaction';
import { AccountService } from '../services/account.service';
import { TransactionService } from '../services/transaction.service';

@Component({
  selector: 'app-generate-otp',
  templateUrl: './generate-otp.component.html',
  styleUrls: ['./generate-otp.component.css'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective }
  ]
})
export class GenerateOtpComponent implements OnInit {

  public generateOTPTxnSubForm = new FormGroup({
    accountId: new FormControl('', [Validators.required]),
    amount: new FormControl('', [Validators.required]),
    selBeneficiary : new FormControl('', [Validators.required]),
    txType: new FormControl('', [Validators.required]),
    remarks: new FormControl('', [Validators.required]),
  });

  public form: FormGroup = new FormGroup({});

  @Output() transactionEmitter = new EventEmitter<Transaction>();

  @Input() bankingRequest : BankingRequest = new BankingRequest();

  public appUserId: string = this.dataSharingService.getAppUserId();
  public appName: string = this.dataSharingService.getAppName();
  public partnerIdNameMap = new Map();
  public partners = new Array<PartnerConfig>();

  public userIdMap = new Map();
  public userStatusList: Array<UserStatus> = new Array<UserStatus>();
  public selectedUser: UserStatus = new UserStatus();
  public selectedAccountCard : AccountCard = new AccountCard();
  public isUserSelectedInfo : boolean = false;

  public transaction: Transaction = new Transaction();

  public errorMessage: string = '';
  public isError: boolean = false;

  public beneficiaryMap = new Map();
  public beneficiaries: Array<Beneficiary> = new Array<Beneficiary>();
  public selectedBeneficiary : Beneficiary = new Beneficiary();
  public isBeneficiarySelectedInfo : boolean = false;

  public isTxTypeSelected : boolean = false;
  public selectedTxTypeInfo : string = '';
  public displayTxType : boolean = true;

  constructor(private transactionService: TransactionService,
    private configService: ConfigService,
    private registrationService: RegistrationService,
    private dataSharingService: DataSharingService,
    private beneficiaryService: BeneficiaryService,
    private accountService : AccountService,
    public dialog : MatDialog,
    private router: Router,
    private ctrlContainer: FormGroupDirective) {

  }

  ngOnInit(): void {
    this.onload();    

    if (this.bankingRequest != undefined && this.bankingRequest.transactionId != undefined) 
    {  
      this.f.amount.setValue(this.bankingRequest.approvalAmount);
      this.f.remarks.setValue(this.bankingRequest.expenseTitle);  
    }

    this.form = this.ctrlContainer.form;
    this.form.addControl("generateOTP", this.generateOTPTxnSubForm);
  }

  onload() : void {
    this.configService.listPartner().subscribe(data => {
      this.partners = data;
      this.partners.forEach(e => {
        this.partnerIdNameMap.set(e.partnerId, e);
      });
    });
    this.registrationService.getAllRegistrations(this.appName, this.appUserId)
      .subscribe(data => {
        this.userStatusList = data;

        this.userStatusList.forEach(e => {
          let key = e.corpId+"-"+e.userId;
          this.userIdMap.set(key, e);
          if (e.primaryAccount) {
            this.autoPopulateForm(key);
            this.selectedUser = e;
            this.isError = false;
            this.errorMessage = '';
            this.isUserSelectedInfo = true;
            this.enableForm();
            this.populateBeneficiaries();
            this.populateAccountBalance();
            //this.onSubmit();
          }
        });
      });
  }

  get f() { return this.generateOTPTxnSubForm.controls; }

  onChange(event: any): void {
    console.log(event);
    let eventKey = event.value;
    
    this.selectedUser = this.userIdMap.get(eventKey);

    console.log(this.selectedUser);
    if (!this.selectedUser.active || this.selectedUser.currentState != 'REGISTERED') {
      this.isError = true;
      this.errorMessage = 'Not Approved or Pending for approval - Go to ICICI Bank Netbanking portal to approve. Note: This is one time activity to register account, thankyou for your patience and operating with us';
      this.isUserSelectedInfo = false;
      this.disableForm();
    } else {
      this.isError = false;
      this.errorMessage = '';
      this.isUserSelectedInfo = true;
      this.enableForm();
      this.populateBeneficiaries();
      this.populateAccountBalance();
    }

  }

  populateAccountBalance() : void {
      let accountReq : Account = new Account();
      accountReq.userId = this.selectedUser.userId;
      accountReq.appUserId = this.appUserId;
      accountReq.appType = Constants.APP_TYPE;
      accountReq.corpId = this.selectedUser.corpId;
      accountReq.partnerId = this.selectedUser.partnerId;
      accountReq.urn = this.selectedUser.urnId;
      accountReq.accountNumber = this.selectedUser.accountNumber;
      this.accountService.accountBalance(accountReq)
      .subscribe(data => {
          let accountResp : Account = data;
          console.log(accountResp); 
          this.selectedAccountCard.accountBalance = accountResp.balance; 
          
          // Display card logo
          this.selectedAccountCard.accountNumber = this.selectedUser.accountNumber;
          let selectedPartner : PartnerConfig = this.partnerIdNameMap.get(this.selectedUser.partnerId);
          this.selectedAccountCard.bankName = selectedPartner.partnerDisplayName;
          this.selectedAccountCard.cardLogoPath = selectedPartner.partnerLogoLocation;
          this.selectedAccountCard.currency = selectedPartner.currency;
          this.selectedAccountCard.cardColor = selectedPartner.partnerThemeColor;
          this.iciciBankRelatedRuleOnUser(selectedPartner);
      });  
  }

  populateBeneficiaries() : void {
    let userId = this.selectedUser.userId!;
    let corpId = this.selectedUser.corpId!;
    this.beneficiaryService
        .getUserBeneficiaries(this.appName, this.appUserId, corpId, userId)
        .subscribe(data => {
            console.log(data)
            this.beneficiaries = data;
            this.beneficiaryMap = new Map();
            
            this.beneficiaries.forEach(e => {
              this.beneficiaryMap.set(e.accountNumber, e);
            });
        });
  }

  generateOTP(): void {
    console.log(this.generateOTPTxnSubForm);
    if (!this.isFormValid()) {
      return;
    }

    this.transaction.appUserId = this.selectedUser.appUserId;
    this.transaction.userId = this.selectedUser.userId;
    this.transaction.appType = this.selectedUser.appType;
    this.transaction.corpId = this.selectedUser.corpId;
    this.transaction.debitAcc = this.selectedUser.accountNumber;
    this.transaction.creditAcc = this.selectedBeneficiary.accountNumber;
    this.transaction.ifscCode = this.selectedBeneficiary.ifscCode;
    this.transaction.amount = this.f.amount.value;

    if (this.selectedBeneficiary.bankName == 'ICICI') {
      this.transaction.txType = Constants.ICICI_BANK_INTRA_BANK_TRANSFER;
    } else {
      this.transaction.txType = this.f.txType.value;
    }
    
    let accBalance = this.selectedAccountCard.accountBalance!;
    let transactionAmount = this.transaction.amount!;
    if (accBalance != undefined && transactionAmount!= undefined && transactionAmount > accBalance) {
      this.isError = true;
      this.errorMessage = "Insufficient balance for this transaction";
      return;
    }

    if (transactionAmount!= undefined && transactionAmount < 1) {
      this.isError = true;
      this.errorMessage = "Please enter more than Re. 1 for this transaction";
      return;
    }
    
    this.transaction.payeeName = this.selectedBeneficiary.name;
    this.transaction.remarks = this.f.remarks.value;
    
    this.transaction.uniqueId = Constants.REFERENCE_ID_PREFIX +
                                Constants.formatDate() + 
                                Constants.getRandomString(14);
    this.transaction.urnId = this.selectedUser.urnId;

    let partnerId = this.selectedUser.partnerId;
    let partnerConfig = this.partnerIdNameMap.get(partnerId);
    this.transaction.currency = partnerConfig.currency;
    this.transaction.partnerId = partnerId;
    
    if (this.bankingRequest != undefined && this.bankingRequest.transactionId != undefined) {
      this.transaction.expenseStatus = this.bankingRequest.status;
    }

    console.log(this.transaction);

    this.transactionService.generateOTP(this.transaction).subscribe(data => {
      console.log(data);
      let error = data["error"];
      if (error != undefined) {
        this.isError = true;
        this.errorMessage = error.errorCode + " = " + error.errorMessage;
      } else {
        this.transactionEmitter.emit(this.transaction);
      }
      //this.router.navigate(['/initiateTxn'], {state: this.transaction});
    });
  }


  isFormValid(): boolean {
    if (this.generateOTPTxnSubForm.invalid) {
      let textMessage = '';
      const controls = this.f;
      console.log(controls);
      for (const name in controls) {
          if (controls[name].invalid) {
            textMessage = "Please enter valid value for : " + name;
            break;
          }
      }

      this.errorMessage = textMessage;
      this.isError = true;
      return false;
    }

    this.isError = false;
    this.errorMessage = ''

    return true;
  }

  onChangeTxType(event: any): void {

  }

  onChangeBeneficiary(event: any): void {
    let beneficiaryAccount = event.value;
    if (beneficiaryAccount != 'None') {
      this.selectedBeneficiary = this.beneficiaryMap.get(beneficiaryAccount);
      this.isBeneficiarySelectedInfo = true;
      this.iciciBankRelatedRule();
    } else {
      this.isBeneficiarySelectedInfo = false;
      this.selectedBeneficiary = new Beneficiary();
    }
  }

  addBeneficiary() : void {
    console.log(this.selectedUser)
    if (this.selectedUser == null || this.selectedUser == undefined || this.selectedUser.userId == undefined) {
      this.isError = true;
      this.errorMessage = 'Please select the user to add beneficiary';
      return;
    }

    const dialogRef = this.dialog.open(CreateBeneficiaryComponent, {
      width : '60%',
      data : { selectedUser : this.selectedUser}
    });
    

    dialogRef.afterClosed().subscribe(result =>{
      this.populateBeneficiaries();
    });

    //this.router.navigate(['/manage_beneficiary']);
  }

  mask(sensitive : string | undefined) : string {
    let format : string = 'XXXX XXXX XXXX';
    let maskedValue : string = '';
    if (sensitive != null && sensitive.length > 6) {
        let actualLength = sensitive.length;
        maskedValue = format.slice(0, actualLength - 2) + sensitive.slice(actualLength-2, actualLength) ;
    }
    return maskedValue;
  }

  public displayICICIBank : boolean = false; 
  iciciBankRelatedRule() : void {
    if (this.selectedBeneficiary.bankName == 'ICICI') 
    {
        this.displayTxType = false;
        this.isTxTypeSelected = false;
        this.f.txType.setValue(Constants.ICICI_BANK_INTRA_BANK_TRANSFER);
    } else {
        this.displayTxType = true;
        this.isTxTypeSelected = true;
    }
  }

  iciciBankRelatedRuleOnUser(selectedPartner : PartnerConfig) : void {
    console.log(selectedPartner);
    if(selectedPartner.partnerName == 'ICICI bank')
    {
        this.displayICICIBank = true;
    } else {
        this.displayICICIBank = false;
    }
  }

  public isUserInActiveORNotRegistered(userStatus : UserStatus) : boolean {
    return !userStatus.active || userStatus.currentState != 'REGISTERED';
  }

  public markDisable : boolean = false;
  public disableForm() : void {
    this.f.amount.disable();
    this.f.txType.disable();
    this.f.remarks.disable();
    this.f.selBeneficiary.disable();
    this.beneficiaryMap = new Map();
    this.markDisable = true;
    this.displayICICIBank = false;
  }

  public enableForm() : void {
    this.f.amount.enable();
    this.f.txType.enable();
    this.f.remarks.enable();
    this.f.selBeneficiary.enable();
    this.markDisable = false;
  }

  public autoPopulateForm(account : string) : void {
    this.f.accountId.setValue(account);
  }

  public cancel() : void {
    this.generateOTPTxnSubForm = new FormGroup({
      accountId: new FormControl('', [Validators.required]),
      amount: new FormControl('', [Validators.required]),
      selBeneficiary : new FormControl('', [Validators.required]),
      txType: new FormControl('', [Validators.required]),
      remarks: new FormControl('', [Validators.required]),
    });
    this.onload();
  }

}
