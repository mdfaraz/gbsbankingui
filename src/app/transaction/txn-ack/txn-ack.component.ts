import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { Router } from '@angular/router';
import { Constants } from 'src/app/core/common/models/constants';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { EnquireTransaction, Transaction } from '../models/transaction';
import { TransactionService } from '../services/transaction.service';

@Component({
  selector: 'app-txn-ack',
  templateUrl: './txn-ack.component.html',
  styleUrls: ['./txn-ack.component.css']
})
export class TxnAckComponent implements OnChanges {

  @Input() public finaltransaction : Transaction = new Transaction();
  enquireTransaction: EnquireTransaction = new EnquireTransaction();

  @Input() stepperData: MatStepper | undefined;
  
  public appUserId: string = this.dataSharingService.getAppUserId();
  public appName: string = this.dataSharingService.getAppName();
  public isError : boolean = false;
  public messageFromBank : string = '';
  public additionalInfo : string = '';

  public txTypeMap : Map<string, string> = Constants.transactionTypeMap();

  constructor(private transactionService : TransactionService,
    private dataSharingService : DataSharingService,
    private router: Router) { 
    }

  ngOnChanges(): void {
    
    this.enquireTransaction.userId = this.finaltransaction.userId;
    this.enquireTransaction.appUserId = this.appUserId;
    this.enquireTransaction.appType = Constants.APP_TYPE;
    this.enquireTransaction.corpId = this.finaltransaction.corpId;
    this.enquireTransaction.userId = this.finaltransaction.userId;
    this.enquireTransaction.urnId = this.finaltransaction.urnId;
    this.enquireTransaction.uniqueId = this.finaltransaction.uniqueId;
    this.enquireTransaction.appType = this.finaltransaction.appType;
    this.enquireTransaction.partnerId = this.finaltransaction.partnerId;
    this.additionalInfo = this.finaltransaction.additionalInfo!;

    console.log(this.enquireTransaction);
    this.transactionService.enquireTxn(this.enquireTransaction)
    .subscribe(data => {
        console.log(data);
        let utrnNumber = data['utrnNumber'];
        if (utrnNumber == this.enquireTransaction.uniqueId)
        {
          this.messageFromBank = 'SUCCESS'
        }
        else {
          let error = data["error"];
          this.messageFromBank = error.errorMessage;
        }
    });
  }

  markDone() : void {
    this.router.navigate(['statement']);
  }

  mask(sensitive : string | undefined) : string {
    let format : string = 'XXXX XXXX XXXX';
    let maskedValue : string = '';
    if (sensitive != null && sensitive.length > 6) {
        let actualLength = sensitive.length;
        maskedValue = format.slice(0, actualLength - 2) + sensitive.slice(actualLength-2, actualLength) ;
    }
    return maskedValue;
  }

  initiateAnotherTxn() : void {
    if (this.stepperData != undefined) {
      this.stepperData.reset();
      //this.form.reset();
    }
  }

  bookSpending() : void {
    this.router.navigate(['costipro']);
  }

}
