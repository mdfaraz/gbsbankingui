import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TxnAckComponent } from './txn-ack.component';

describe('TxnAckComponent', () => {
  let component: TxnAckComponent;
  let fixture: ComponentFixture<TxnAckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TxnAckComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TxnAckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
