import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-template',
  templateUrl: './dashboard-template.component.html',
  styleUrls: ['./dashboard-template.component.css']
})
export class DashboardTemplateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public accountBalanceDisplayColumns : string[] = ['sno', 'bank', 'accountNumber', 'userId', 'balance', 'pay'];

  public last10txnsDisplayColumns : string[] = ['sno', 'txnId', 'paidfrom', 'paidTo', 'amount', 'status'];

  public total10BeneficiariesDisplayColumns : string[] = ['sno', 'payee', 'totalAmount', 'totalTransactions'];

  public totalPaidDisplayColumns : string[] = ['sno', 'bank', 'accountNumber', 'userId', 'amountPaid'];

}
