import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardTemplateComponent } from './dashboard-template/dashboard-template.component';
import { DashboardCardComponent } from './dashboard-card/dashboard-card.component';



@NgModule({
  declarations: [
    DashboardTemplateComponent,
    DashboardCardComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DashboardCardComponent,
    DashboardTemplateComponent
  ]
})
export class DashboardModule { }
