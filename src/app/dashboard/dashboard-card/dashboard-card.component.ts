import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { BankingSnapshot } from '../models/banking-snapshot';
import { InfoletsReq } from '../models/infolets-req';
import { InfoletsService } from '../services/infolets.service';

@Component({
  selector: 'app-dashboard-card',
  templateUrl: './dashboard-card.component.html',
  styleUrls: ['./dashboard-card.component.css']
})
export class DashboardCardComponent implements OnInit, OnChanges {

  @Input() public title: string = '';

  @Input() public type: string = '';

  @Input() public headerClass: string = '';

  @Input() public displayedColumns: string[] = new Array<string>();

  dataSource = new MatTableDataSource<BankingSnapshot>();

  constructor(private infoletService: InfoletsService, 
    private dataSharingService: DataSharingService) { }

  ngOnInit() {
    this.populateSnapshotForTable();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.displayedColumns);
    this.populateSnapshotForTable();
  }

  populateSnapshotForTable() : void {
    if (this.type == 'accountBalance') {
      this.populateSnapshotForAccountsTable();
    }
    if (this.type == 'last10Txns') {
    this.populateSnapshotForLast10TxnsTable();
    }
    if (this.type == 'total10Payee') {
    this.populateSnapshotForTop10PayeeTable();
    }
    if (this.type == 'totalPaidAccontPerMonth') {
    this.populateSnapshotFortotalPaidTable();
    }
  }

  populateSnapshotForAccountsTable(): void {
    this.displayedColumns =  ['sno', 'bank', 'accountNumber', 'userId', 'balance'];
    this.dataSource = new MatTableDataSource<BankingSnapshot>();
    let infoletsReq = new InfoletsReq();
    infoletsReq.appName = this.dataSharingService.getAppName();
    infoletsReq.appUserId = this.dataSharingService.getAppUserId();
    infoletsReq.numOfRecords = 10;
    infoletsReq.appType = 'WEB';
    infoletsReq.requestType = 'ACCOUNT_BALANCE_PER_ACCOUNT';
    this.infoletService.fetchTableDetail(infoletsReq)
        .subscribe(data => {
          let bankingSnapshots : Array<BankingSnapshot> = data;
          this.dataSource = new MatTableDataSource<BankingSnapshot>(bankingSnapshots); 
        });
  }

  populateSnapshotForLast10TxnsTable(): void {
    this.displayedColumns =  ['sno', 'txnId', 'paidfrom', 'paidTo', 'amount', 'status'];
    let infoletsReq = new InfoletsReq();
    infoletsReq.appName = this.dataSharingService.getAppName();
    infoletsReq.appUserId = this.dataSharingService.getAppUserId();
    infoletsReq.numOfRecords = 10;
    infoletsReq.appType = 'WEB';
    infoletsReq.requestType = 'LAST_N_TRANSACTION';
    this.infoletService.fetchTableDetail(infoletsReq)
        .subscribe(data => {
          let bankingSnapshots : Array<BankingSnapshot> = data;
          this.dataSource = new MatTableDataSource<BankingSnapshot>(bankingSnapshots); 
        });
  }

  populateSnapshotForTop10PayeeTable(): void {
    this.displayedColumns =  ['sno', 'payee', 'totalAmount', 'totalTransactions'];
    let infoletsReq = new InfoletsReq();
    infoletsReq.appName = this.dataSharingService.getAppName();
    infoletsReq.appUserId = this.dataSharingService.getAppUserId();
    infoletsReq.numOfRecords = 10;
    infoletsReq.appType = 'WEB';
    infoletsReq.requestType = 'TOP_N_PAYEE';
    this.infoletService.fetchTableDetail(infoletsReq)
        .subscribe(data => {
          let bankingSnapshots : Array<BankingSnapshot> = data;
          this.dataSource = new MatTableDataSource<BankingSnapshot>(bankingSnapshots); 
        });
  }

  populateSnapshotFortotalPaidTable(): void {
    this.displayedColumns =  ['sno', 'bank', 'accountNumber', 'userId', 'amountPaid'];
    this.dataSource = new MatTableDataSource<BankingSnapshot>();
    let infoletsReq = new InfoletsReq();
    infoletsReq.appName = this.dataSharingService.getAppName();
    infoletsReq.appUserId = this.dataSharingService.getAppUserId();
    infoletsReq.numOfRecords = 10;
    infoletsReq.appType = 'WEB';
    infoletsReq.requestType = 'TOTAL_PAYMENTS_PER_MONTH';
    this.infoletService.fetchTableDetail(infoletsReq)
        .subscribe(data => {
          let bankingSnapshots : Array<BankingSnapshot> = data;
          this.dataSource = new MatTableDataSource<BankingSnapshot>(bankingSnapshots); 
        });
  }

  /*
public last10txnsDisplayColumns : string[] = ['sno', 'txnId', 'paidfrom', 'paidTo', 'amount', 'status'];

  public total10BeneficiariesDisplayColumns : string[] = ['sno', 'payee', 'totalAmount', 'totalTransactions'];

  public totalPaidDisplayColumns : string[] = ['sno', 'bank', 'accountNumber', 'userId', 'amountPaid'];
  */

}
