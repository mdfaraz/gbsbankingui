export class BankingSnapshot {
    sno? : string;
    bank? : string;
    accountNumber? : string;
    userId? : string;
    balance? : number;
    pay? : number;
    txnId? : string;
    paidfrom? : string;
    paidTo? : string;
    amount? : number;
    status? : string;
    payee? : string;
    totalAmount? : number;
    totalTransactions? : number;
    amountPaid? : number;
}
