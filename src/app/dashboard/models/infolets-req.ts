export class InfoletsReq {
    appUserId? : string;
    appName? : string;
    appType? : string;
    requestType? : string;
    numOfRecords? : number;
}
