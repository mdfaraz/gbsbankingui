import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { InfoletsReq } from '../models/infolets-req';

@Injectable({
  providedIn: 'root'
})
export class InfoletsService {

  private baseUrl = environment.baseUrl+'/api/v1/infolets';

  constructor(private http : HttpClient) { }

  fetchTableDetail(infoletsReq: InfoletsReq): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID()
    };  
    return this.http.post(this.baseUrl + `/table/detail`, infoletsReq, {headers});
  }
}
