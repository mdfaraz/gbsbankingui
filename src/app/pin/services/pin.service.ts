import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/core/common/models/constants';
import { environment } from 'src/environments/environment';
import { PinReq } from '../models/pin';
import { Crypto } from 'src/app/core/common/services/crypto';


@Injectable({
  providedIn: 'root'
})
export class PinService {

  private baseUrl = environment.baseUrl+'/api/v1/pin';

  constructor(private http : HttpClient) { }

  createPinApi(createPin: PinReq): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(createPin))
    }; 
    console.log(headers);
    return this.http.post(this.baseUrl + `/create`, createPin, {headers});
  }

  verifyPinApi(verifyPin: PinReq): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(verifyPin))
    }; 
    console.log(headers);
    return this.http.post(this.baseUrl + `/verify`, verifyPin, {headers});
  }

  isNewPinRequest(appName:string , appUserId: string): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID()
    }; 
    let currentTimestamp  = Date.now();
    let queryParams = "?ts="+currentTimestamp;
    return this.http.get(this.baseUrl + `/validate/`+appName+"/"+appUserId+queryParams, {headers});
  }

  resetPinApi(resetPin: PinReq, oldPin : string): Observable<any> {
    const headers = {
      'Content-type': 'application/json;charset=utf-8',
      'txRef': UUID.UUID(),
      'signature' : Crypto.getSignature(Crypto.getMessage(resetPin))
    }; 
    console.log(headers);
    return this.http.post(this.baseUrl + `/reset/`+oldPin, resetPin, {headers});
  }

}
