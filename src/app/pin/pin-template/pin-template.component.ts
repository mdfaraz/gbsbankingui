import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BankingRequest } from 'src/app/core/common/models/banking';
import { Constants } from 'src/app/core/common/models/constants';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { PinResponse } from '../models/pin';
import { PinService } from '../services/pin.service';

@Component({
  selector: 'app-pin-template',
  templateUrl: './pin-template.component.html',
  styleUrls: ['./pin-template.component.css']
})
export class PinTemplateComponent implements OnInit {

  public initCreateNewPinFlow : boolean = true;
  
  public appUserId : string = this.dataSharingService.getAppUserId();
  public appName : string = this.dataSharingService.getAppName();
  public errorMessage : string = '';
  public isError : boolean = false;

  public bankingRequest : BankingRequest = new BankingRequest();

  constructor(private pinService : PinService,
              private dataSharingService : DataSharingService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.activatedRoute.paramMap.subscribe(params => {
      console.log(params);
      const enryptedStr = params.get('encryptedStr')!;
      const decodedStr = atob(enryptedStr);
      //console.log(decodedStr);
      this.bankingRequest = JSON.parse(decodedStr);
      const appName = this.bankingRequest.appName;
      const appUserId = this.bankingRequest.appUserId;
      this.appUserId = appUserId != null ? appUserId : this.appUserId;
      this.appName = appName != null ? appName : this.appName;
      this.pinService.isNewPinRequest(this.appName, this.appUserId)
      .subscribe(data => {
          let pinResp : PinResponse = data;
          console.log(pinResp);

          let error = pinResp.error;
          if (error != undefined) {
            this.isError = true;
            this.errorMessage = error.errorMessage!;
          } else {
            this.initCreateNewPinFlow = pinResp.valid != undefined ? pinResp.valid : false;
            this.dataSharingService.initializeLoginUser(this.appUserId, this.appName);
            this.isError = false;
            this.errorMessage = ''
          }  
      })
    });
  }
}
