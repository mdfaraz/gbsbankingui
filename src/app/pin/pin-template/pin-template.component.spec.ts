import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PinTemplateComponent } from './pin-template.component';

describe('PinTemplateComponent', () => {
  let component: PinTemplateComponent;
  let fixture: ComponentFixture<PinTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PinTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PinTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
