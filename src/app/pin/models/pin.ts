import { ErrorMsg } from "src/app/core/common/models/error";

export class PinReq {
    appUserId? : string;
    appName? : string;
    pin? : string;
}

export class PinResponse {
    valid? : boolean;
    error? : ErrorMsg;
}