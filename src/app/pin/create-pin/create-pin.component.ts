import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Constants } from 'src/app/core/common/models/constants';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { PinReq, PinResponse } from '../models/pin';
import { PinService } from '../services/pin.service';

@Component({
  selector: 'app-create-pin',
  templateUrl: './create-pin.component.html',
  styleUrls: ['./create-pin.component.css']
})
export class CreatePinComponent implements OnInit {

  public createPinForm = new FormGroup({
    newPinValue: new FormControl('', [Validators.required]),
    reNewPinValue: new FormControl('', [Validators.required])
  });

  createPinReq: PinReq = new PinReq();
  appUserId: string = this.dataSharingService.getAppUserId();
  appName: string = this.dataSharingService.getAppName();

  public errorMessage: string = '';
  public isError: boolean = false;
  public hide = true;

  constructor(private pinService: PinService,
    private dataSharingService: DataSharingService,
    private router: Router) { }

  ngOnInit(): void {

  }

  get f() { return this.createPinForm.controls; }

  onSubmit() {
      console.log("appName=["+this.appName+"] --- appUserId=["+this.appUserId+"]");

      if (this.createPinForm.invalid) {
        let textMessage = '';
        const controls = this.f;
        for (const name in controls) {
            if (controls[name].invalid) {
              textMessage = "Please enter valid value for : " + name;
              break;
            }
        }
  
        this.errorMessage = textMessage;
        this.isError = true;
        return;
      }

      this.isError = false;
      this.errorMessage = ''
      if (this.appName == null || this.appName == undefined) {
         this.isError = true;
         this.errorMessage = 'App Name is invalid'
      }

      if (this.appUserId == null || this.appUserId == undefined) {
        this.isError = true;
        this.errorMessage = 'App User Id is invalid'
     }


      let newPin = this.f.newPinValue.value;
      let confirmPin = this.f.reNewPinValue.value;
      if (newPin == confirmPin) 
      {
        this.createPinReq.appUserId = this.appUserId;
        this.createPinReq.appName = this.appName;
        this.createPinReq.pin = newPin;
        this.pinService.createPinApi(this.createPinReq)
          .subscribe(data => {

            let pinResp: PinResponse = data;
            console.log(pinResp);

            let error = pinResp.error;
            if (error != undefined) {
              this.isError = true;
              this.errorMessage = error.errorMessage!;
            } else {
              this.isError = false;
              this.errorMessage = ''
              this.router.navigate(['/verifyPin'])
            }
          })
      }
      else {
        this.isError = true;
        this.errorMessage = 'Pin not matched';
      }
}

}
