import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BankingRequest } from 'src/app/core/common/models/banking';
import { Constants } from 'src/app/core/common/models/constants';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { User } from 'src/app/registration/models/user';
import { RegistrationService } from 'src/app/registration/services/registration.service';
import { PinReq, PinResponse } from '../models/pin';
import { PinService } from '../services/pin.service';

@Component({
  selector: 'app-verify-pin',
  templateUrl: './verify-pin.component.html',
  styleUrls: ['./verify-pin.component.css']
})
export class VerifyPinComponent implements OnInit {

  public verifyPinForm = new FormGroup({
    confirmPinValue: new FormControl('', [Validators.required])
  });

  @Input() bankingRequest : BankingRequest = new BankingRequest();

  public users : Array<User> = new Array<User>();
  public appUserId : string = this.dataSharingService.getAppUserId();
  public appName : string = this.dataSharingService.getAppName();
  verifyPinReq: PinReq = new PinReq();
  public errorMessage: string = '';
  public isError: boolean = false;
  public hide = true;

  constructor(private pinService: PinService,
              private dataSharingService : DataSharingService,
              private registrationService : RegistrationService,
              private router: Router) { }

  ngOnInit(): void {
    console.log("----ngChild -----");
    console.log(this.bankingRequest);
  }

  get f() { return this.verifyPinForm.controls; }

  onSubmit() {
    let confirmPin = this.f.confirmPinValue.value;

    if (this.verifyPinForm.invalid) {
      let textMessage = '';
      const controls = this.f;
      for (const name in controls) {
          if (controls[name].invalid) {
            textMessage = "Please enter valid value for : " + name;
            break;
          }
      }

      this.errorMessage = textMessage;
      this.isError = true;
      return;
    }

    this.verifyPinReq.appUserId = this.appUserId;
    this.verifyPinReq.appName = this.appName;
    this.verifyPinReq.pin = confirmPin;
    console.log(this.verifyPinReq);
    this.pinService.verifyPinApi(this.verifyPinReq)
      .subscribe(data => {

        let pinResp: PinResponse = data;
        console.log(pinResp);

        let error = pinResp.error;
        if (error != undefined) {
          this.isError = true;
          this.errorMessage = error.errorMessage!;
        } 
        else 
        {
          if (pinResp.valid) {
            this.isError = false;
            this.errorMessage = ''
            this.dataSharingService.updateUserLoggedIn();
            this.registrationService.getAllRegistrations(this.appName,
              this.appUserId).subscribe(d => {
                  this.users = d;
                  if(this.users.length == 0) {
                    this.router.navigate(['/register-landing'])
                  } else {
                    if (this.bankingRequest.transactionId != undefined) {
                      let jsonString = JSON.stringify(this.bankingRequest);
                      let encodedStr = btoa(jsonString);
                      this.router.navigate(['/txn-train'], { queryParams: {'expenseItem' : encodedStr}});
                    } else {
                      this.router.navigate(['/statement'])
                    }
                  }
              });
            
          }
          else {
            this.isError = true;
            this.errorMessage = 'Invalid Pin !! Please try again';
          }
        }
      });
  } 
  
  redirectToResetPin() : void {
    this.router.navigate(['reset-pin']);
  }
}
