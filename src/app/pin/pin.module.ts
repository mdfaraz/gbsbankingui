import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreatePinComponent } from './create-pin/create-pin.component';
import { VerifyPinComponent } from './verify-pin/verify-pin.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import { PinTemplateComponent } from './pin-template/pin-template.component';
import { ResetPinComponent } from './reset-pin/reset-pin.component';
//import { MatButtonModule, MatCardModule, MatSidenavModule, MatFormFieldModule, MatInputModule, MatTooltipModule, MatToolbarModule,MatGridListModule, MatTableModule, MatCheckboxModule, MatDialogModule, MatSlideToggleModule, MatRadioModule } from '@angular/material';



@NgModule({
  declarations: [
    CreatePinComponent,
    VerifyPinComponent,
    PinTemplateComponent,
    ResetPinComponent
  ],
  imports: [
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    CommonModule,
    FormsModule,
    RouterModule,    
    HttpClientModule
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    CreatePinComponent,
    VerifyPinComponent,
    PinTemplateComponent,
    ResetPinComponent
  ]
})
export class PinModule { }
