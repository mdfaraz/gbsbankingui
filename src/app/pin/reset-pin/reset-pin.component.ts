import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataSharingService } from 'src/app/core/common/services/data-sharing.service';
import { PinReq, PinResponse } from '../models/pin';
import { PinService } from '../services/pin.service';

@Component({
  selector: 'app-reset-pin',
  templateUrl: './reset-pin.component.html',
  styleUrls: ['./reset-pin.component.css']
})
export class ResetPinComponent implements OnInit {

  public resetPinForm = new FormGroup({
    oldPinValue: new FormControl('', [Validators.required]),
    newPinValue: new FormControl('', [Validators.required])
  });

  resetPinReq: PinReq = new PinReq();
  appUserId: string = this.dataSharingService.getAppUserId();
  appName: string = this.dataSharingService.getAppName();

  public errorMessage: string = '';
  public isError: boolean = false;
  public hide = true;

  constructor(private pinService: PinService,
    private dataSharingService: DataSharingService,
    private router: Router) { }

  ngOnInit(): void {

  }

  get f() { return this.resetPinForm.controls; }

  onSubmit() : void {

    if (this.resetPinForm.invalid) {
      let textMessage = '';
      const controls = this.f;
      for (const name in controls) {
          if (controls[name].invalid) {
            textMessage = "Please enter valid value for : " + name;
            break;
          }
      }

      this.errorMessage = textMessage;
      this.isError = true;
      return;
    }

    this.isError = false;
      this.errorMessage = ''
      if (this.appName == null || this.appName == undefined) {
         this.isError = true;
         this.errorMessage = 'App Name is invalid'
      }

      if (this.appUserId == null || this.appUserId == undefined) {
        this.isError = true;
        this.errorMessage = 'App User Id is invalid'
     }


      let oldPin = this.f.oldPinValue.value;
      let newPin = this.f.newPinValue.value;

      this.resetPinReq.appUserId = this.appUserId;
      this.resetPinReq.appName = this.appName;
      this.resetPinReq.pin = newPin;
      
      this.pinService.resetPinApi(this.resetPinReq, oldPin)
          .subscribe(data => {

            let pinResp: PinResponse = data;
            console.log(pinResp);

            let error = pinResp.error;
            if (error != undefined) {
              this.isError = true;
              this.errorMessage = error.errorMessage!;
            } else {
              this.isError = false;
              this.errorMessage = ''
              this.router.navigate(['/verifyPin'])
            }
          });
  }

  cancelReset() : void {
    this.router.navigate(['verifyPin']);
  }

}
