import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import { FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list';
import { RouterModule } from '@angular/router';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

import { CreatePinComponent } from './pin/create-pin/create-pin.component';
import { VerifyPinComponent } from './pin/verify-pin/verify-pin.component';
import { CreateRegistrationComponent } from './registration/create-registration/create-registration.component';
import { RegistrationService } from './registration/services/registration.service';
import { RegistrationListComponent } from './registration/registration-list/registration-list.component';
import { GenerateOtpComponent } from './transaction/generate-otp/generate-otp.component';
import { CreateTransactionComponent } from './transaction/create-transaction/create-transaction.component';
import { TxnTrainComponent } from './transaction/txn-train/txn-train.component';
import { NavComponent } from './core/nav/nav.component';
import { HeaderComponent } from './core/header/header.component';
import { TemplateComponent } from './core/template/template.component';
import { TransactionService } from './transaction/services/transaction.service';
import { StatementService } from './statement/services/statement.service';
import { ConfigService } from './core/common/services/config.service';
import { StatementListComponent } from './statement/statement-list/statement-list.component';
import { SearchPanelComponent } from './statement/search-panel/search-panel.component';
import { StatementPaginationComponent } from './statement/statement-pagination/statement-pagination.component';
import { StatementTemplateComponent } from './statement/statement-template/statement-template.component';
import { MatNativeDateModule } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { FormService } from './transaction/services/form.service';
import { TxnAckComponent } from './transaction/txn-ack/txn-ack.component';
import { PinService } from './pin/services/pin.service';
import { PinTemplateComponent } from './pin/pin-template/pin-template.component';
import { DataSharingService } from './core/common/services/data-sharing.service';
import { CreateBeneficiaryComponent } from './registration/create-beneficiary/create-beneficiary.component';
import { BeneficiaryListComponent } from './registration/beneficiary-list/beneficiary-list.component';
import { LogoutComponent } from './core/logout/logout.component';
import { AccountCardComponent } from './core/account-card/account-card.component';
import { RegistrationLandingComponent } from './registration/registration-landing/registration-landing.component';
import { RegistrationTemplateComponent } from './registration/registration-template/registration-template.component';
import { RegistrationStepsComponent } from './registration/registration-steps/registration-steps.component';
import { HelpDialogBoxComponent } from './core/help-dialog-box/help-dialog-box.component';
import { AckComponent } from './registration/ack/ack.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { CallbackComponent } from './core/callback/callback.component';
import { ContactUsComponent } from './core/contact-us/contact-us.component';
import { AddBeneficiaryComponent } from './registration/add-beneficiary/add-beneficiary.component';
import { ContactUsPopupComponent } from './core/contact-us-popup/contact-us-popup.component';
import {APP_BASE_HREF} from '@angular/common';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';

import { BnNgIdleService } from 'bn-ng-idle';
import { LoginGuard } from './core/guard/login.guard';
import { DisableRightClickDirective } from './core/directives/disable-right-click.directive';
import { DisableCopyPasteDirective } from './core/directives/disable-copy-paste.directive';
import { BackButtonDisableModule } from 'angular-disable-browser-back-button';
import { DashboardTemplateComponent } from './dashboard/dashboard-template/dashboard-template.component';
import { DashboardCardComponent } from './dashboard/dashboard-card/dashboard-card.component';
import { ResetPinComponent } from './pin/reset-pin/reset-pin.component';

export function httpTranslateLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    TemplateComponent,
    NavComponent,
    HeaderComponent,
    CreatePinComponent,
    VerifyPinComponent,
    PinTemplateComponent,
    CreateRegistrationComponent,
    RegistrationListComponent,
    GenerateOtpComponent,
    CreateTransactionComponent,
    TxnTrainComponent,
    SearchPanelComponent,
    StatementListComponent,
    StatementPaginationComponent,
    StatementTemplateComponent,
    TxnAckComponent,
    CreateBeneficiaryComponent,
    BeneficiaryListComponent,
    LogoutComponent,
    AccountCardComponent,
    RegistrationLandingComponent,
    RegistrationTemplateComponent,
    RegistrationStepsComponent,
    CallbackComponent,
    AckComponent,
    ContactUsComponent,
    AddBeneficiaryComponent,
    ContactUsPopupComponent,
    DisableRightClickDirective,
    DisableCopyPasteDirective,
    DashboardTemplateComponent,
    DashboardCardComponent,
    ResetPinComponent
  ],
  entryComponents: [
    HelpDialogBoxComponent
  ],
  imports: [
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    BrowserModule,
    MatSelectModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    BrowserAnimationsModule,
    MatGridListModule,
    HttpClientModule,
    MatTableModule,
    MatTabsModule,
    MatListModule,
    RouterModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatStepperModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    BackButtonDisableModule.forRoot()
  ],
  exports: [
    MatFormFieldModule
  ],
  providers: [
    RegistrationService,
    StatementService,
    TransactionService,
    ConfigService,
    DatePipe,
    FormService,
    FormGroupDirective,
    PinService,
    DataSharingService,
    BnNgIdleService,
    LoginGuard
    //{provide: APP_BASE_HREF, useValue: '/banking'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
