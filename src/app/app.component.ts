import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BnNgIdleService } from 'bn-ng-idle';
import { Constants } from './core/common/models/constants';
import { ConfirmationDialogComponent } from './core/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'gbs-banking-ui';

  createPinActivated : boolean = true;
  verifyPinActivated : boolean = false;

  constructor(private bnIdle: BnNgIdleService,
    private dialog : MatDialog,
    private router : Router)
  {
    // Register translation languages
    //translate.addLangs(['en', 'es', 'fr']);
    // Set default language
    //translate.setDefaultLang('en');
  }  

  ngOnInit(): void {
    this.bnIdle.startWatching(Constants.IDLE_TIMEOUT)
    .subscribe((isTimedOut: boolean) => {
      if (isTimedOut) {
        //console.log('session expired');
        this.alertForSessionExpiration();
      }
    });
  }

  alertForSessionExpiration(): void {
    const dRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: { message: "Your session has is idle ! Click <b>Yes</b> to continue." }
    });
    dRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result != undefined) {
          if (result.data) {
            console.log('Yes clicked');
          } else {
            console.log('No clicked');
            this.router.navigate(['costipro']);
          }
      } else {
        console.log('Clicked nothing .. session expiring');
        this.router.navigate(['costipro']);
      }
      
    });
  }

}
