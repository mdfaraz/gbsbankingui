### STAGE 1: Build ###
FROM node:14.17.6 as build-step
LABEL OWNER="dogeshwar.yadav@gmail.com"
ENV SERVICE_NAME gbs-banking-client
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm cache clean --force
RUN npm install -g @angular/cli@12.2.9
RUN npm install
COPY . .
RUN npm run build

### STAGE 2: Run ###
FROM nginx:1.17.1-alpine
COPY nginx.conf  /etc/nginx/conf.d/default.conf
COPY --from=build-step /app/dist/gbs-banking-ui /usr/share/nginx/html
EXPOSE 80